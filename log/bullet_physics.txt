A engine esta utilizando a versão do bullet 3.6
baixada pelo proprio sistema:

    sudo apt-get install libbullet-dev

CMakeLists:

    find_package(Bullet REQUIRED)
    include_directories(${BULLET_INCLUDE_DIR})
    target_link_libraries(${BULLET_LIBRARIES})
