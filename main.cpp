#include <iostream>
#include <queue>
#include <EngineCore.h>

#include <Serialize/BulletWorldImporter/btBulletWorldImporter.h>

#define SNAKE_LEFT  glm::vec2(-1,  0)
#define SNAKE_RIGHT glm::vec2( 1,  0)
#define SNAKE_UP    glm::vec2( 0,  1)
#define SNAKE_DOWN  glm::vec2( 0, -1)
#define SNAKE_INITIAL_POSITION glm::vec3(0, 0, 0)

enum
{
    GROUND_OBJECT,
    FRUIT_OBJECT,
    PLAYER_OBJECT
};

Engine engine(1280, 720);
EntityObject ground;
EntityObject fruit;
EntityObject tree;

struct {
    glm::vec2 direction{0.0f};
    EntityObject entity;
    btRigidBody* rbSnake;
    void setDirection(glm::vec2 direction)
    {
        this->direction = direction;
    }
    
    btVector3 getBtDirection()
    {
        return {direction.x, 0, -direction.y};
    }

} snake;

std::vector<glm::vec3> treePositions = {
    glm::vec3{20, -8, -20},
    glm::vec3{-20, -8, 0},
};
// ------------------- LUMINAÇÃO ----------------//
DirectionalLight light{1.0, {0, 20, 0}, {1.0, 1.0, 1.0}};
// ------------------- SHADERS -----------------//
Shader DefaultShader;
Shader LightShader;

// ------------------ TEXTURES ---------------//
Texture2D GroundTexture;
Texture2D FruitTexture;
Texture2D TreeTexture;
Texture2D SnakeTexture;
// -------- FISICA --------- //
btWorldImporter* importer;
btAlignedObjectArray<btCollisionShape*> collisionShapes;

void loadShaders()
{
    DefaultShader   = LOAD_SHADER("model", NULL, "DefaultShader");
    LightShader     = LOAD_SHADER("directional_light", NULL, "LightShader");
}

void loadTextures()
{
    GroundTexture   = LOAD_TEXTURE("dark_green_solid.png",  true, "GroundTexture");
    FruitTexture    = LOAD_TEXTURE("apple.jpg",             false, "FoodTexture");
    TreeTexture     = LOAD_TEXTURE("tree_texture.001.png", true, "TreeTexture");
    SnakeTexture    = LOAD_TEXTURE("assets/textures/dark_green_solid.png", true, "SnakeTexture");
}

void initGroundPhysics(btVector3 position)
{
    btTriangleMesh* tm = std::static_pointer_cast<AssimpModel>(ground.getModel3D())->btMesh;
    btCollisionShape* groundShape = new btBvhTriangleMeshShape(tm, true);
    collisionShapes.push_back(groundShape);

    btTransform groundTransform;
    groundTransform.setIdentity();
    groundTransform.setOrigin(position);
    btScalar mass(0.);
    btVector3 localInertia(0, 0, 0);
    
    btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
    btRigidBody* rbGround = new btRigidBody(rbInfo);
    rbGround->setUserIndex(GROUND_OBJECT);
    engine.getBtDynamicsWorld()->addRigidBody(rbGround);
}

void initSpherePhysics(btVector3 position)
{
    btCollisionShape* colShape = importer->createSphereShape(btScalar(1.));    
    collisionShapes.push_back(colShape);

    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(position);

    btScalar mass(2.f);
    btVector3 localInertia(0, 0, 0);
    colShape->calculateLocalInertia(mass, localInertia);

    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* rbFood = new btRigidBody(rbInfo);
    rbFood->setUserIndex(FRUIT_OBJECT);
    engine.getBtDynamicsWorld()->addRigidBody(rbFood);
}

void initPlayerSpherePhysics(btVector3 position)
{
    btCollisionShape* colShape = importer->createSphereShape(btScalar(1.));    
    collisionShapes.push_back(colShape);

    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(position);

    btScalar mass(2.f);
    btVector3 localInertia(0, 0, 0);
    // colShape->calculateLocalInertia(mass, localInertia);

    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* rbFood = new btRigidBody(rbInfo);
    rbFood->setUserIndex(PLAYER_OBJECT);
    engine.getBtDynamicsWorld()->addRigidBody(rbFood);
    snake.rbSnake = rbFood;
}

int main(void)
{
    importer = new btWorldImporter(engine.getBtDynamicsWorld());

    loadShaders();
    loadTextures();

    // carregando modelos
    ground.loadModel("assets/objects/terrain02/terrain02.obj");
    fruit.loadModel("assets/objects/sphere/sphere02.obj");
    snake.entity.loadModel("assets/objects/sphere/sphere02.obj");

    tree.loadModel("assets/objects/tree01_texture/terrain03.obj");
    tree.getTransform()->setScale(glm::vec3{2.0f});

    engine.setDebugMode(true);
    
    initGroundPhysics({0, -10, 0});
    initSpherePhysics({0, 20, 0});
    initPlayerSpherePhysics({0, 5, 0});

    Engine::getCamera().setPosition(glm::vec3(0, 0, 40));    
    // Engine::getCamera().setOrbitalCamera(true);

    glm::vec3 camPos = Engine::getCameraPosition();

    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
            
        Engine::getCamera().setPosition(camPos);    

        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }
        
        if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
            snake.setDirection(SNAKE_LEFT);
        } 
        
        if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
            snake.setDirection(SNAKE_RIGHT);
        }

        if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
            snake.setDirection(SNAKE_UP);
        }

        if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
            snake.setDirection(SNAKE_DOWN);
        }

        snake.rbSnake->applyCentralImpulse(snake.getBtDirection() * 0.2f);

    });

    engine.setUpdateCallback([&](float dt) -> void {

    });

    engine.setRenderCallback([&](float dt) -> void {
        
        { // ImGui
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();                
                ImGui::Begin("Game");
                    ImGui::Text("Light");   
                    ImGui::SliderFloat("Intensity", &light.intensity, 0.0f, 1.0f);
                    ImGui::SliderFloat3("Light Position", &light.direction[0], -100.0f, 100.0f);
                    ImGui::ColorPicker3("Color", &light.color[0]);
                    ImGui::Separator();
                    ImGui::Text("Camera");  
                    ImGui::SliderFloat3("Camera Position", &camPos[0], -100.0f, 100.0f);
                    ImGui::Separator();
                    ImGui::Text("Objetos");  
                    if(ImGui::Button("Criar Esfera"))
                        initSpherePhysics({0, 20, 0});
                ImGui::End();
            ImGui::EndFrame();
        }
        
        btDynamicsWorld* world = engine.getBtDynamicsWorld();
        for (int j = 0; j < world->getNumCollisionObjects(); j++)
		{
			btCollisionObject* obj = world->getCollisionObjectArray()[j];
			btRigidBody* body = btRigidBody::upcast(obj);
			btTransform trans;

			if (body && body->getMotionState())
			{
                body->getMotionState()->getWorldTransform(trans);     
                if(body->getUserIndex() == GROUND_OBJECT)
                {
                    LightShader.bind();
                    LightShader.setDirectionalLight(light);
                    GroundTexture.bind();
                    ground.getTransform()->setIdentityMatrix();
                    ground.getTransform()->setPosition(bt3glm(trans.getOrigin()));                    
                    ground.getTransform()->setRotate(trans.getRotation());
                    ground.render(DefaultShader);
                } else if(body->getUserIndex() == FRUIT_OBJECT) {
                    btSphereShape* sp = (btSphereShape*)body->getCollisionShape();
                    DefaultShader.bind();
                    FruitTexture.bind();
                    fruit.getTransform()->setIdentityMatrix();
                    fruit.getTransform()->setPosition(bt3glm(trans.getOrigin()));                    
                    fruit.getTransform()->setRotate(trans.getRotation());
                    fruit.getTransform()->setScale(glm::vec3{sp->getRadius()});
                    fruit.render(DefaultShader);
                } else if(body->getUserIndex() == PLAYER_OBJECT) {
                    btSphereShape* sp = (btSphereShape*)body->getCollisionShape();
                    LightShader.bind();
                    LightShader.setDirectionalLight(light);
                    SnakeTexture.bind();
                    snake.entity.getTransform()->setIdentityMatrix();
                    snake.entity.getTransform()->setPosition(bt3glm(trans.getOrigin()));                    
                    snake.entity.getTransform()->setRotate(trans.getRotation());
                    snake.entity.getTransform()->setScale(glm::vec3{sp->getRadius()});
                    snake.entity.render(LightShader);
                }
            }
        }

        // arvore
        LightShader.bind();
        LightShader.setDirectionalLight(light);
        for(auto position : treePositions)
        {
            TreeTexture.bind();
            tree.getTransform()->setIdentityMatrix();
            tree.getTransform()->setPosition(position);
            tree.render(DefaultShader);
        }


    });

    engine.run();

    return 0;
}