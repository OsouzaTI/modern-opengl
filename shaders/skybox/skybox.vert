#version 330 core
layout (location = 0) in vec3 inPosition;

out vec3 UV;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    gl_Position = projection * view * model * vec4(inPosition, 1.0f);
    UV = inPosition;
}