#version 330 core

in vec3 UV;

uniform samplerCube SkyBox;

void main()
{
    gl_FragColor = texture(SkyBox, UV);
}