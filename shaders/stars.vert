#version 330 core
layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;
  
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float time;

out float Time;
out vec2 UV;
out vec3 Position;

void main()
{
    Time = time;
    UV = inUV;
    Position = inPosition;
    gl_Position = projection * view * model * vec4(inPosition, 1.0);
}
