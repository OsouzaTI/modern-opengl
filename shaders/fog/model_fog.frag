#version 330 core

in vec2 UV;
// position of vertex relative to world
// model * vertex
in vec4 Position;

uniform vec3 cameraEye;
uniform vec4 fogColor;
uniform sampler2D TextureSampler;

const float density = 1/100.0f;
const float gradient = 1.1;

void main()
{
    
    float distance = length(cameraEye - Position.xyz);
    float visibility = exp(-pow( (distance * density), gradient ));
    visibility = clamp(visibility, 0.0, 1.0);

    vec4 color = texture(TextureSampler, UV);
    gl_FragColor = mix(fogColor, color, visibility);
}