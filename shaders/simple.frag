#version 330

in vec2 UV;

out vec4 Color;

uniform sampler2D TextureSampler;

void main()
{
    Color = texture2D(TextureSampler, UV);
}