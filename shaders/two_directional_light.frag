#version 330 core

uniform sampler2D TextureSampler;

uniform float intensityA;
uniform vec3 lightA;
uniform vec3 lightColorA;

uniform float intensityB;
uniform vec3 lightB;
uniform vec3 lightColorB;

uniform float intensityC;
uniform vec3 lightC;
uniform vec3 lightColorC;

uniform float intensityD;
uniform vec3 lightD;
uniform vec3 lightColorD;

// in vec3 colorTest;
in vec4 Position;
in vec3 Normal;
in vec2 UV;
in float Time;

out vec4 outColor;

vec3 lightDiffuse(float intensity, vec3 light, vec3 lightColor) {

    float ambient = 0.2;

    // normaliza a normal
    vec3 N = normalize(Normal);
    // calcula o vetor de direção da luz (Lfinal - Pinicial) vetor PL .
    vec3 L = normalize(light - Position.xyz);
    // produto escalar entre o vetor NORMAL e o vetor de direção da LUZ
    float lambertian = dot(N, L);
    lambertian = clamp(lambertian, 0.0, 1.0);

    /* Calculando o especular : propriedade do espaço de camera,
        O observador sempre está em olhando em diração (0, 0, -1).
    */

    vec3 eye = vec3(0.0, 0.0, -1.0);
    // vetor inverso do observador
    vec3 invEye = -eye;
    // Calculando vetor de reflexão utilizando o shader
    vec3 reflection = reflect(-L, N);
    // termo especular (Reflection . invEye) ^ k
    float k = 10.0;

    // produto escalar: reflection . invEye
    float dotReflectInvEye = dot(reflection, invEye);
    // calculando o expoente constante K
    float specularReflection = pow(dotReflectInvEye, k);
    // removendo valores negativos
    specularReflection = max(0.0, specularReflection);

    vec3 textureColor = texture(TextureSampler, UV).rgb;
    vec3 DiffuseReflection = lambertian * intensity * textureColor + specularReflection * ambient;

    return DiffuseReflection * lightColor;
}

void main()
{   
    vec3 lightDiffuseA = lightDiffuse(intensityA, lightA, lightColorA);
    vec3 lightDiffuseB = lightDiffuse(intensityB, lightB, lightColorB);
    vec3 lightDiffuseC = lightDiffuse(intensityC, lightC, lightColorC);
    vec3 lightDiffuseD = lightDiffuse(intensityD, lightD, lightColorD);
    outColor = vec4(lightDiffuseA + lightDiffuseB, 1.0);
}