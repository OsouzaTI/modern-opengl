#version 330 core

out vec4 Color;

uniform sampler2D TextureSampler;

in float Time;
in vec2 UV;
in vec3 Position;

void main()
{
    Color = texture(TextureSampler, UV + Time * 0.002);
} 