#version 330 core
layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;

// Posição do vertice no espaço da camera (ViewMatrix * ModelMatrix * Vertice)
out vec4 Position;
out vec3 Normal;
out vec2 UV;

// out vec3 colorTest;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform float intensity;
uniform vec3 light;
uniform float time;

out float Time;

mat4 calculateNormalMatrix() 
{
    return inverse(transpose(view * model));
}

vec3 lightAlbedo(mat4 NormalMatrix)
{
    float ambient = 0.2;
    float diffuseAlbedo = 0.8;
    // calculando as coodenadas do ponto em espaço de camera
    vec4 P = view * model * vec4(inPosition, 1.0f);
    // normalizando a normal convertida em espaço de camera
    vec3 N = normalize(vec3(NormalMatrix * vec4(inNormal, 0.0)));
    // obtendo o vetor normalizado da direção da luz
    vec3 L = normalize(light - P.xyz);
    // produto escalar entre a normal e a direção da luz
    float NL = max(dot(N, L), 0.0);
    // cor do modelo
    vec3 modelColor = vec3(1.0, 0.0, 0.0);
    // cor final com luz aplicada
    return clamp(modelColor * ambient + modelColor * NL * diffuseAlbedo, 0.0, 1.0);
}

void main()
{
    mat4 NormalMatrix = calculateNormalMatrix();
    UV = inUV;
    Normal = vec3(NormalMatrix * vec4(inNormal, 0.0));
    Position = view * model * vec4(inPosition, 1.0f);
    // lightAlbedo(NormalMatrix);
    Time = time;

    gl_Position = projection * Position;
}