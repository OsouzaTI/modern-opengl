#version 330 core

uniform sampler2D TextureSampler;

uniform float intensity;
uniform vec3 light;
uniform vec3 lightColor;

// in vec3 colorTest;
in vec4 Position;
in vec3 Normal;
in vec2 UV;
in float Time;

out vec4 outColor;

vec3 lightDiffuse() {

    float ambient = 0.2;

    // normaliza a normal
    vec3 N = normalize(Normal);
    // calcula o vetor de direção da luz (Lfinal - Pinicial) vetor PL .
    vec3 L = normalize(light - Position.xyz);
    // produto escalar entre o vetor NORMAL e o vetor de direção da LUZ
    float lambertian = dot(N, L);
    lambertian = clamp(lambertian, 0.0, 1.0);

    /* Calculando o especular : propriedade do espaço de camera,
        O observador sempre está em olhando em diração (0, 0, -1).
    */

    vec3 eye = vec3(0.0, 0.0, -1.0);
    // vetor inverso do observador
    vec3 invEye = -eye;
    // Calculando vetor de reflexão utilizando o shader
    vec3 reflection = reflect(-L, N);
    // termo especular (Reflection . invEye) ^ k
    float k = 10.0;

    // produto escalar: reflection . invEye
    float dotReflectInvEye = dot(reflection, invEye);
    // calculando o expoente constante K
    float specularReflection = pow(dotReflectInvEye, k);
    // removendo valores negativos
    specularReflection = max(0.0, specularReflection);

    vec3 textureColor = texture(TextureSampler, UV).rgb;
    vec3 DiffuseReflection = lambertian * intensity * textureColor + specularReflection * ambient;

    return DiffuseReflection * lightColor;
}

void main()
{   
    outColor = vec4(lightDiffuse(), 1.0);
}