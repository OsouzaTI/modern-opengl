#version 330 core
layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;

// Posição do vertice no espaço da camera (ViewMatrix * ModelMatrix * Vertice)
out vec2 UV;
out vec3 WorldPos;
out vec3 Normal;

// out vec3 colorTest;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    UV = inUV;
    WorldPos = vec3(model * vec4(inPosition, 1.0));
    Normal = mat3(model) * inNormal;   
    gl_Position = projection * view * vec4(WorldPos, 1.0);
}