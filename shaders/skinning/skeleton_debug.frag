#version 330 core

in vec2 UV;
flat in ivec4 BoneIds;
in vec4 Weights;
out vec4 Color;

uniform sampler2D TextureSampler;
uniform int boneToRender;

void main()
{
    bool found = false;
    for(int i = 0; i < 4; i++)
    {
        if(BoneIds[i] == boneToRender) {
            if(Weights[i] >= 0.7) {
                Color = vec4(1.0, 0.0, 0.0, 1.0) * Weights[i];
            } else if (Weights[i] >= 0.4 && Weights[i] <= 0.6) {
                Color = vec4(0.0, 1.0, 0.0, 1.0) * Weights[i];
            } else if (Weights[i] >= 0.1) {
                Color = vec4(1.0, 1.0, 0.0, 1.0) * Weights[i];
            }
            found = true;
            break;
        }
    }

    if(!found) {
        Color = texture(TextureSampler, UV);
    } 

}