#version 330

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec3 inNormal;
layout (location = 2) in vec2 inUV;
layout (location = 3) in int[4] inBoneIds;
layout (location = 4) in float[4] inWeights;

out vec2 UV;
flat out ivec4 BoneIds;
out vec4 Weights;

const int MAX_BONES = 100;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 bones[MAX_BONES];

void main()
{
    mat4 boneTransform  =   bones[inBoneIds[0]] * inWeights[0];
    boneTransform       +=  bones[inBoneIds[1]] * inWeights[1];
    boneTransform       +=  bones[inBoneIds[2]] * inWeights[2];
    boneTransform       +=  bones[inBoneIds[3]] * inWeights[3];    

    vec4 vertexBone = boneTransform * vec4(inPosition, 1.0f);
    gl_Position = projection * view * model * vertexBone;
    UV = inUV;
    BoneIds = ivec4(inBoneIds[0], inBoneIds[1], inBoneIds[2], inBoneIds[3]);
    Weights = vec4(inWeights[0], inWeights[1], inWeights[2], inWeights[3]);
}