#ifndef ASSIMP_MODEL_H
#define ASSIMP_MODEL_H

#include <Structures/Mesh.h>
#include <Core/Shader.h>
#include <Model/Model.h>
#include <iostream>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <stb/stb_image.h>
#include <bullet/btBulletDynamicsCommon.h>

unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma = false);

class AssimpModel : public Model
{
public:
    AssimpModel();
        
    void loadModel(std::string path);
    void process();
    void update(float deltaTime);
    void render(Shader &shader);
    void render(glm::mat4 viewMatix, Shader& shader) {};

    inline std::vector<Vertex> getVertices() const {
        return meshes[0].getVertices();
    };

    inline std::vector<unsigned int> getIndices() const {
        return meshes[0].getIndices();
    };

    // Integração do Assimp com Bullet Physics
    btTriangleMesh* btMesh;

private:

    // ASSIMP FUNCTIONS
    std::vector<SimpleTexture> texturesLoaded;        
    std::vector<Mesh> meshes;        
    std::string directory;

    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<SimpleTexture> loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName);
};

#endif