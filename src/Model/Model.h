#ifndef MODEL_H
#define MODEL_H

#include <memory>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Components/Collider.h>
#include <Components/BoxCollider.h>
#include <Core/ResourceManager.h>
#include <Core/EngineObject.h>
#include <Structures/Vertex.hpp>
#include <Components/Collider.h>

class Model : public EngineObject {
protected:
    MinMaxPoints minMaxPoints;
public:
    Model() {};    
    virtual void loadModel(std::string path) = 0;
    virtual std::vector<Vertex> getVertices() const = 0;
    virtual std::vector<unsigned int> getIndices() const = 0;
    inline MinMaxPoints& getMinMaxPoints() { return minMaxPoints; };
};

#endif