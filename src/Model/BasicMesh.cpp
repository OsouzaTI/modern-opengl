#include <Model/BasicMesh.h>

BasicMesh::BasicMesh()
{
    
}

BasicMesh::~BasicMesh()
{
    
}

void BasicMesh::clear()
{
    if (buffers[0] != 0) {
        glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(buffers), buffers);
    }

    if (VAO != 0) {
        glDeleteVertexArrays(1, &VAO);
        VAO = 0;
    }
}

bool BasicMesh::loadMesh(const std::string& file)
{
    // limpa da memoria a malha carregada anteriormente
    clear();

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // criando os buffers para os atributos de vertices
    glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(buffers), buffers);
    
    scene = importer.ReadFile(file.c_str(), ASSIMP_LOAD_FLAGS);
    if(scene) {           
        initFromScene(scene, file);
    } else {
        std::cout << "Não foi possivel ler o arquivo de objeto" << std::endl;
    }

    glBindVertexArray(0);

    return true;

}

void BasicMesh::initFromScene(const aiScene* scene, const std::string& file)
{
    meshes.resize(scene->mNumMeshes);
    materials.resize(scene->mNumMaterials);

    uint numVertices    = 0;
    uint numIndices     = 0;

    countVerticesAndIndices(scene, numVertices, numIndices);
    reserveSpace(numVertices, numIndices);
    initAllMeshes(scene);
    initMaterials(scene, file);
    populateBuffers();

}

void BasicMesh::countVerticesAndIndices(const aiScene* scene, uint& numVertices, uint& numIndices)
{
    for (uint i = 0; i < meshes.size(); i++)
    {
        meshes[i].materialIndex = scene->mMeshes[i]->mMaterialIndex;
        meshes[i].numIndices = scene->mMeshes[i]->mNumFaces * 3;
        meshes[i].baseVertex = numVertices;
        meshes[i].baseIndex = numIndices;
        numVertices += scene->mMeshes[i]->mNumVertices;
        numIndices  += meshes[i].numIndices;
    }
    
}

void BasicMesh::reserveSpace(uint numVertices, uint numIndices)
{
    positions.reserve(numVertices);
    normals.reserve(numVertices);
    texCoords.reserve(numVertices);
    indices.reserve(numIndices);
}

void BasicMesh::initAllMeshes(const aiScene* scene)
{
    for (uint i = 0; i < meshes.size(); i++)
    {
        const aiMesh* mesh = scene->mMeshes[i];
        initSingleMesh(i, mesh);
    }
    
}

void BasicMesh::initSingleMesh(uint meshIndex, const aiMesh* mesh)
{
    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    for (uint i = 0; i < mesh->mNumVertices; i++)
    {
        const aiVector3D& position  = mesh->mVertices[i];
        const aiVector3D& texCoord = mesh->HasTextureCoords(0) ?  mesh->mTextureCoords[0][i] : Zero3D;
        positions.push_back(glm::vec3(position.x, position.y, position.z));
        texCoords.push_back(glm::vec2(texCoord.x, texCoord.y));

        if(mesh->HasNormals()) {
            const aiVector3D& normal    = mesh->mNormals[i];
            normals.push_back(glm::vec3(normal.x, normal.y, normal.z));
        }    
    
    }
    
    // processando os indices da malha
    for (uint i = 0; i < mesh->mNumFaces; i++)
    {
        const aiFace& face = mesh->mFaces[i];
        // assert(face.mNumIndices == 3);
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }
    

}

void BasicMesh::initMaterials(const aiScene* scene, const std::string& file)
{
    std::string::size_type slashIndex = file.find_last_of("/") ;
    std::string directory;

    if(slashIndex == std::string::npos) {
        directory = ".";
    } else if(slashIndex == 0) {
        directory = "/";
    } else {
        directory = file.substr(0, slashIndex);
    }

    // inicializando os materiais
    for (uint i = 0; i < scene->mNumMaterials; i++) {
        const aiMaterial* material = scene->mMaterials[i];
        loadTextures(directory, material, i);
    }
    

}

void BasicMesh::loadTextures(const std::string& directory, const aiMaterial* material, int index)
{
    loadDiffuseTexture(directory, material, index);
    LoadSpecularTexture(directory, material, index);
}

void BasicMesh::loadDiffuseTexture(const std::string& directory, const aiMaterial* material, int materialIndex)
{
    materials[materialIndex].diffuse = nullptr;
    if(material->GetTextureCount(aiTextureType_DIFFUSE) > 0)
    {
        aiString path;
        if(material->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) 
        {
            // verificando a existencia de sub texturas
            const aiTexture* aiTexture = scene->GetEmbeddedTexture(path.C_Str());
            if(aiTexture) {
                loadDiffuseTextureEmbedded(aiTexture, materialIndex);
            } else {
                loadDiffuseTextureFromFile(directory, path, materialIndex);
            }
        }
    }
}

void BasicMesh::loadDiffuseTextureEmbedded(const aiTexture* texture, int materialIndex)
{
    std::cout << "Embedded diffuse texture type " << texture->achFormatHint << std::endl;
    materials[materialIndex].diffuse = new Texture(GL_TEXTURE_2D);
    uint bufferSize = texture->mWidth;
    materials[materialIndex].diffuse->load(bufferSize, texture->pcData);
}

void BasicMesh::loadDiffuseTextureFromFile(const std::string& directory, const aiString& path, int materialIndex)
{
    std::string p(path.data);
    for (int i = 0 ; i < p.length() ; i++) {
        if (p[i] == '\\') {
            p[i] = '/';
        }
    }

    if (p.substr(0, 2) == ".\\") {
        p = p.substr(2, p.size() - 2);
    }

    std::string fullPath = directory + "/" + p;

    materials[materialIndex].diffuse = new Texture(GL_TEXTURE_2D, fullPath.c_str());
    if(!materials[materialIndex].diffuse->load()) {
        std::cout << "Erro ao carregar textura difusa " << fullPath << std::endl;
    } else {
        std::cout << "Textura difusa carregada: " << fullPath << " at " << materialIndex << std::endl;
    }

}

void BasicMesh::LoadSpecularTexture(const std::string& directory, const aiMaterial* material, int materialIndex)
{
    materials[materialIndex].specular = nullptr;
    if(material->GetTextureCount(aiTextureType_SHININESS) > 0)
    {
        aiString path;
        if(material->GetTexture(aiTextureType_SHININESS, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) 
        {
            // verificando a existencia de sub texturas
            const aiTexture* aiTexture = scene->GetEmbeddedTexture(path.C_Str());
            if(aiTexture) {
                LoadSpecularTextureEmbedded(aiTexture, materialIndex);
            } else {
                LoadSpecularTextureFromFile(directory, path, materialIndex);
            }
        }
    }
}

void BasicMesh::LoadSpecularTextureEmbedded(const aiTexture* texture, int materialIndex)
{
    std::cout << "Embedded specular texture type " << texture->achFormatHint << std::endl;
    materials[materialIndex].specular = new Texture(GL_TEXTURE_2D);
    uint bufferSize = texture->mWidth;
    materials[materialIndex].specular->load(bufferSize, texture->pcData);
}

void BasicMesh::LoadSpecularTextureFromFile(const std::string& directory, const aiString& path, int materialIndex)
{
    std::string p(path.data);
    for (int i = 0 ; i < p.length() ; i++) {
        if (p[i] == '\\') {
            p[i] = '/';
        }
    }

    if (p.substr(0, 2) == ".\\") {
        p = p.substr(2, p.size() - 2);
    }

    std::string fullPath = directory + "/" + p;

    materials[materialIndex].specular = new Texture(GL_TEXTURE_2D, fullPath.c_str());
    if(!materials[materialIndex].specular->load()) {
        std::cout << "Erro ao carregar textura specular " << fullPath << std::endl;
    } else {
        std::cout << "Textura specular carregada: " << fullPath << " at " << materialIndex << std::endl;
    }
}

void BasicMesh::populateBuffers() 
{
    // Buffer de vertices
    glBindBuffer(GL_ARRAY_BUFFER, buffers[POSITION_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(POSITION_LOCATION);
    glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, buffers[NORMAL_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * normals.size(), &normals[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(NORMAL_LOCATION);
    glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[TEXCOORD_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(TEX_COORD_LOCATION);
    glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[INDEX_BUFFER]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

}


void BasicMesh::render()
{
    glBindVertexArray(VAO);
    
    for (uint i = 0; i < meshes.size(); i++)
    {
        uint materialIndex = meshes[i].materialIndex;
        assert(materialIndex < materials.size());

        if(materials[materialIndex].diffuse) {
            materials[materialIndex].diffuse->bind(GL_TEXTURE0);
        }

        if(materials[materialIndex].specular) {
            materials[materialIndex].specular->bind(GL_TEXTURE1);
        }

        glDrawElementsBaseVertex(
            GL_TRIANGLES, 
            meshes[i].numIndices, 
            GL_UNSIGNED_INT,
            (void*)(sizeof(uint) * meshes[i].baseIndex),
            meshes[i].baseVertex
        );

    }
    
    glBindVertexArray(0);
}
