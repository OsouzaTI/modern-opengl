#include <Model/SkinnedMesh.h>

SkinnedMesh::SkinnedMesh()
{
    
}

SkinnedMesh::~SkinnedMesh()
{
    
}

void SkinnedMesh::clear()
{
    if (buffers[0] != 0) {
        glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(buffers), buffers);
    }

    if (VAO != 0) {
        glDeleteVertexArrays(1, &VAO);
        VAO = 0;
    }
}

uint SkinnedMesh::numOfBones() const
{
    return (uint)boneNameToIndexMap.size();
}

bool SkinnedMesh::loadMesh(const std::string& file)
{
    // limpa da memoria a malha carregada anteriormente
    clear();

    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    // criando os buffers para os atributos de vertices
    glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(buffers), buffers);
    
    scene = importer.ReadFile(file.c_str(), ASSIMP_LOAD_FLAGS);
    if(scene) {   
        globalInverseTransform = mat4_cast(scene->mRootNode->mTransformation);
        globalInverseTransform = glm::inverse(globalInverseTransform);
        initFromScene(scene, file);
    } else {
        std::cout << "Não foi possivel ler o arquivo de objeto" << std::endl;
    }

    glBindVertexArray(0);

    return true;

}

void SkinnedMesh::initFromScene(const aiScene* scene, const std::string& file)
{
    meshes.resize(scene->mNumMeshes);
    materials.resize(scene->mNumMaterials);

    uint numVertices    = 0;
    uint numIndices     = 0;

    countVerticesAndIndices(scene, numVertices, numIndices);
    reserveSpace(numVertices, numIndices);
    initAllMeshes(scene);
    initMaterials(scene, file);
    populateBuffers();

}

void SkinnedMesh::countVerticesAndIndices(const aiScene* scene, uint& numVertices, uint& numIndices)
{
    for (uint i = 0; i < meshes.size(); i++)
    {
        meshes[i].materialIndex = scene->mMeshes[i]->mMaterialIndex;
        meshes[i].numIndices = scene->mMeshes[i]->mNumFaces * 3;
        meshes[i].baseVertex = numVertices;
        meshes[i].baseIndex = numIndices;
        numVertices += scene->mMeshes[i]->mNumVertices;
        numIndices  += meshes[i].numIndices;
    }
    
}

void SkinnedMesh::reserveSpace(uint numVertices, uint numIndices)
{
    positions.reserve(numVertices);
    normals.reserve(numVertices);
    texCoords.reserve(numVertices);
    indices.reserve(numIndices);
    bones.resize(numVertices);
}

void SkinnedMesh::initAllMeshes(const aiScene* scene)
{
    for (uint i = 0; i < meshes.size(); i++)
    {
        const aiMesh* mesh = scene->mMeshes[i];
        initSingleMesh(i, mesh);
    }
    
}

void SkinnedMesh::initSingleMesh(uint meshIndex, const aiMesh* mesh)
{
    const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);

    for (uint i = 0; i < mesh->mNumVertices; i++)
    {
        const aiVector3D& position  = mesh->mVertices[i];
        const aiVector3D& normal    = mesh->mNormals[i];
        const aiVector3D& texCoord = mesh->HasTextureCoords(0) ?  mesh->mTextureCoords[0][i] : Zero3D;
        positions.push_back(glm::vec3(position.x, position.y, position.z));
        normals.push_back(glm::vec3(normal.x, normal.y, normal.z));
        texCoords.push_back(glm::vec2(texCoord.x, texCoord.y));
    }
    
    loadMeshBones(meshIndex, mesh);

    // processando os indices da malha
    for (uint i = 0; i < mesh->mNumFaces; i++)
    {
        const aiFace& face = mesh->mFaces[i];
        assert(face.mNumIndices == 3);
        indices.push_back(face.mIndices[0]);
        indices.push_back(face.mIndices[1]);
        indices.push_back(face.mIndices[2]);
    }
    

}

void SkinnedMesh::initMaterials(const aiScene* scene, const std::string& file)
{
    std::string::size_type slashIndex = file.find_last_of("/") ;
    std::string directory;

    if(slashIndex == std::string::npos) {
        directory = ".";
    } else if(slashIndex == 0) {
        directory = "/";
    } else {
        directory = file.substr(0, slashIndex);
    }

    // inicializando os materiais
    for (uint i = 0; i < scene->mNumMaterials; i++) {
        const aiMaterial* material = scene->mMaterials[i];
        loadTextures(directory, material, i);
    }
    

}

void SkinnedMesh::loadTextures(const std::string& directory, const aiMaterial* material, int index)
{
    loadDiffuseTexture(directory, material, index);
    LoadSpecularTexture(directory, material, index);
}

void SkinnedMesh::loadDiffuseTexture(const std::string& directory, const aiMaterial* material, int materialIndex)
{
    materials[materialIndex].diffuse = nullptr;
    if(material->GetTextureCount(aiTextureType_DIFFUSE) > 0)
    {
        aiString path;
        if(material->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) 
        {
            // verificando a existencia de sub texturas
            const aiTexture* aiTexture = scene->GetEmbeddedTexture(path.C_Str());
            if(aiTexture) {
                loadDiffuseTextureEmbedded(aiTexture, materialIndex);
            } else {
                loadDiffuseTextureFromFile(directory, path, materialIndex);
            }
        }
    }
}

void SkinnedMesh::loadDiffuseTextureEmbedded(const aiTexture* texture, int materialIndex)
{
    std::cout << "Embedded diffuse texture type " << texture->achFormatHint << std::endl;
    materials[materialIndex].diffuse = new Texture(GL_TEXTURE_2D);
    uint bufferSize = texture->mWidth;
    materials[materialIndex].diffuse->load(bufferSize, texture->pcData);
}

void SkinnedMesh::loadDiffuseTextureFromFile(const std::string& directory, const aiString& path, int materialIndex)
{
    std::string p(path.data);
    for (int i = 0 ; i < p.length() ; i++) {
        if (p[i] == '\\') {
            p[i] = '/';
        }
    }

    if (p.substr(0, 2) == ".\\") {
        p = p.substr(2, p.size() - 2);
    }

    std::string fullPath = directory + "/" + p;

    materials[materialIndex].diffuse = new Texture(GL_TEXTURE_2D, fullPath.c_str());
    if(!materials[materialIndex].diffuse->load()) {
        std::cout << "Erro ao carregar textura difusa " << fullPath << std::endl;
    } else {
        std::cout << "Textura difusa carregada: " << fullPath << " at " << materialIndex << std::endl;
    }

}

void SkinnedMesh::LoadSpecularTexture(const std::string& directory, const aiMaterial* material, int materialIndex)
{
    materials[materialIndex].specular = nullptr;
    if(material->GetTextureCount(aiTextureType_SHININESS) > 0)
    {
        aiString path;
        if(material->GetTexture(aiTextureType_SHININESS, 0, &path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS) 
        {
            // verificando a existencia de sub texturas
            const aiTexture* aiTexture = scene->GetEmbeddedTexture(path.C_Str());
            if(aiTexture) {
                LoadSpecularTextureEmbedded(aiTexture, materialIndex);
            } else {
                LoadSpecularTextureFromFile(directory, path, materialIndex);
            }
        }
    }
}

void SkinnedMesh::LoadSpecularTextureEmbedded(const aiTexture* texture, int materialIndex)
{
    std::cout << "Embedded specular texture type " << texture->achFormatHint << std::endl;
    materials[materialIndex].specular = new Texture(GL_TEXTURE_2D);
    uint bufferSize = texture->mWidth;
    materials[materialIndex].specular->load(bufferSize, texture->pcData);
}

void SkinnedMesh::LoadSpecularTextureFromFile(const std::string& directory, const aiString& path, int materialIndex)
{
    std::string p(path.data);
    for (int i = 0 ; i < p.length() ; i++) {
        if (p[i] == '\\') {
            p[i] = '/';
        }
    }

    if (p.substr(0, 2) == ".\\") {
        p = p.substr(2, p.size() - 2);
    }

    std::string fullPath = directory + "/" + p;

    materials[materialIndex].specular = new Texture(GL_TEXTURE_2D, fullPath.c_str());
    if(!materials[materialIndex].specular->load()) {
        std::cout << "Erro ao carregar textura specular " << fullPath << std::endl;
    } else {
        std::cout << "Textura specular carregada: " << fullPath << " at " << materialIndex << std::endl;
    }
}


void SkinnedMesh::loadMeshBones(uint meshIndex, const aiMesh* mesh)
{
    for (uint i = 0; i < mesh->mNumBones; i++)
    {
        loadSingleBone(meshIndex, mesh->mBones[i]);
    }
    
}

void SkinnedMesh::loadSingleBone(uint meshIndex, const aiBone* bone)
{
    int boneId = getBoneId(bone);

    if(boneId == boneInfo.size()) {
        BoneInfo bi(mat4_cast(bone->mOffsetMatrix));
        boneInfo.push_back(bi);
    }

    for (uint i = 0; i < bone->mNumWeights; i++)
    {
        const aiVertexWeight& vw = bone->mWeights[i];
        uint globalVertexId = meshes[meshIndex].baseVertex + bone->mWeights[i].mVertexId;
        bones[globalVertexId].addBoneData(boneId, vw.mWeight);
    }    
}

int SkinnedMesh::getBoneId(const aiBone* bone)
{
    int boneId = 0;
    std::string boneName(bone->mName.C_Str());
    if(boneNameToIndexMap.find(boneName) == boneNameToIndexMap.end())
    {
        // adiciona o novo osso ao mapa
        boneId = (int)boneNameToIndexMap.size();
        boneNameToIndexMap[boneName] = boneId;
    } else {
        boneId = boneNameToIndexMap[boneName];
    }

    return boneId;
}

uint SkinnedMesh::getNumAnimations()
{
    if(scene) {
        return scene->mNumAnimations;
    }

    return 0;
}

bool SkinnedMesh::hasAnimations() const {
    if(scene) {
        return scene->HasAnimations();
    }
    return false;
}

void SkinnedMesh::setAnimation(uint animationId)
{
    if(animationId >= 0 && animationId < getNumAnimations()) {
        this->animationId = animationId;
    }
}

void SkinnedMesh::getBoneTransforms(float timeInSeconds, std::vector<glm::mat4>& transforms)
{
    glm::mat4 identity(1.0f);

    uint numPosKeys = scene->mAnimations[animationId]->mChannels[0]->mNumPositionKeys;
    double animationDuration = scene->mAnimations[animationId]->mChannels[0]->mPositionKeys[numPosKeys - 1].mTime;
    double animationTicksPerSecond = scene->mAnimations[animationId]->mTicksPerSecond;
    float ticksPerSecond = (float)(
        animationTicksPerSecond != 0 
            ? animationTicksPerSecond
            : 25.0f
    );

    float timeInTicks = timeInSeconds * ticksPerSecond;
    float animationTimeTicks = fmod(timeInTicks, animationDuration);

    transforms.resize(boneInfo.size());
    readNodeHeirarchy(animationTimeTicks, scene->mRootNode, identity);
    for (uint i = 0; i < boneInfo.size(); i++)
    {
        transforms[i] = boneInfo[i].finalTransformation;
    }
    
}

void SkinnedMesh::readNodeHeirarchy(float animationTimeInTicks, const aiNode* node, const glm::mat4& parentTransform)
{
    std::string nodeName(node->mName.data);

    glm::mat4 nodeTransformation = mat4_cast(node->mTransformation);
    
    const aiAnimation* animation = scene->mAnimations[animationId];
    const aiNodeAnim* nodeAnimation = findNodeAnimation(animation, nodeName);

    if(nodeAnimation)
    {
        
        aiVector3D scaling;
        calculateInterpolatedScaling(scaling, animationTimeInTicks, nodeAnimation);
        glm::vec3 scale{scaling.x, scaling.y, scaling.z};
        glm::mat4 scalingMatrix = glm::scale(glm::mat4(1.0f), scale);

        aiQuaternion rotationQ;
        calculateInterpolatedRotation(rotationQ, animationTimeInTicks, nodeAnimation);
        glm::quat rotation = quat_cast(rotationQ);
        glm::mat4 rotationMatrix = glm::toMat4(rotation);

        aiVector3D translation;
        calculateInterpolatedPosition(translation, animationTimeInTicks, nodeAnimation);
        glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), glm::vec3{translation.x, translation.y, translation.z});

        nodeTransformation = translationMatrix * rotationMatrix * scalingMatrix;

    }

    glm::mat4 globalTransformation = parentTransform * nodeTransformation;

    if(boneNameToIndexMap.find(nodeName) != boneNameToIndexMap.end())
    {
        uint boneIndex = boneNameToIndexMap[nodeName];
        boneInfo[boneIndex].finalTransformation = globalInverseTransform * globalTransformation * boneInfo[boneIndex].offsetMatrix;
    }

    for (uint i = 0; i < node->mNumChildren; i++)
    {
        readNodeHeirarchy(animationTimeInTicks, node->mChildren[i], globalTransformation);
    }
    
}

const aiNodeAnim* SkinnedMesh::findNodeAnimation(const aiAnimation* animation, const std::string nodeName)
{
    for (uint i = 0; i < animation->mNumChannels; i++)
    {
        const aiNodeAnim* nodeAnimation = animation->mChannels[i];
        if(std::string(nodeAnimation->mNodeName.data) == nodeName) {
            return nodeAnimation;
        }
    }
    return nullptr;
}

uint SkinnedMesh::findScaling(float animationTimeInTicks, const aiNodeAnim* nodeAnimation)
{
    assert(nodeAnimation->mNumScalingKeys > 0);
    for (uint i = 0; i < nodeAnimation->mNumScalingKeys - 1; i++)
    {
        float t = (float)nodeAnimation->mScalingKeys[i + 1].mTime;
        if(animationTimeInTicks < t) {
            return i;
        }
    }
    return 0;
}

uint SkinnedMesh::findRotation(float animationTimeInTicks, const aiNodeAnim* nodeAnimation)
{
    assert(nodeAnimation->mNumRotationKeys > 0);
    for (uint i = 0; i < nodeAnimation->mNumRotationKeys - 1; i++)
    {
        float t = (float)nodeAnimation->mRotationKeys[i + 1].mTime;
        if(animationTimeInTicks < t) {
            return i;
        }
    }
    return 0;
}

uint SkinnedMesh::findPosition(float animationTimeInTicks, const aiNodeAnim* nodeAnimation)
{
    assert(nodeAnimation->mNumPositionKeys > 0);
    for (uint i = 0; i < nodeAnimation->mNumPositionKeys - 1; i++)
    {
        float t = (float)nodeAnimation->mPositionKeys[i + 1].mTime;
        if(animationTimeInTicks < t) {
            return i;
        }
    }
    return 0;
}

void SkinnedMesh::calculateInterpolatedScaling(aiVector3D& out, float animationInTimeTicks, const aiNodeAnim* nodeAnimation)
{
    if(nodeAnimation->mNumScalingKeys == 1) {
        out = nodeAnimation->mScalingKeys[0].mValue;
        return;
    }

    uint scalingIndex = findScaling(animationInTimeTicks, nodeAnimation);
    uint nextScalingIndex = scalingIndex + 1;
    assert(nextScalingIndex < nodeAnimation->mNumScalingKeys);
    float t1 = (float)nodeAnimation->mScalingKeys[scalingIndex].mTime;
    float t2 = (float)nodeAnimation->mScalingKeys[nextScalingIndex].mTime;
    float deltaTime = t2 - t1;
    float factor = (animationInTimeTicks - (float)t1) / deltaTime;
    assert(factor >= 0.0f && factor <= 1.0f);
    const aiVector3D& start = nodeAnimation->mScalingKeys[scalingIndex].mValue;
    const aiVector3D& end   = nodeAnimation->mScalingKeys[nextScalingIndex].mValue;
    aiVector3D delta = end - start;
    out = start + factor * delta;
}

void SkinnedMesh::calculateInterpolatedRotation(aiQuaternion& out, float animationInTimeTicks, const aiNodeAnim* nodeAnimation)
{
    if(nodeAnimation->mNumRotationKeys == 1) {
        out = nodeAnimation->mRotationKeys[0].mValue;
        return;
    }

    uint rotationIndex = findRotation(animationInTimeTicks, nodeAnimation);
    uint nextRotationIndex = rotationIndex + 1;
    assert(nextRotationIndex < nodeAnimation->mNumRotationKeys);

    float t1 = (float)nodeAnimation->mRotationKeys[rotationIndex].mTime;
    float t2 = (float)nodeAnimation->mRotationKeys[nextRotationIndex].mTime;

    float deltaTime = t2 - t1;
    float factor = (animationInTimeTicks - t1) / deltaTime;

    assert(factor >= 0.0f && factor <= 1.0f);

    const aiQuaternion& startRotation = nodeAnimation->mRotationKeys[rotationIndex].mValue;
    const aiQuaternion& endRotation = nodeAnimation->mRotationKeys[nextRotationIndex].mValue;
    aiQuaternion::Interpolate(out, startRotation, endRotation, factor);
    out.Normalize();
}

void SkinnedMesh::calculateInterpolatedPosition(aiVector3D& out, float animationInTimeTicks, const aiNodeAnim* nodeAnimation)
{
    if(nodeAnimation->mNumPositionKeys == 1) {
        out = nodeAnimation->mPositionKeys[0].mValue;
        return;
    }

    uint positionIndex = findPosition(animationInTimeTicks, nodeAnimation);
    uint nextPositionIndex = positionIndex + 1;
    assert(nextPositionIndex < nodeAnimation->mNumPositionKeys);

    float t1 = (float)nodeAnimation->mPositionKeys[positionIndex].mTime;
    float t2 = (float)nodeAnimation->mPositionKeys[nextPositionIndex].mTime;
    float deltaTime = t2 - t1;
    float factor = (animationInTimeTicks - t1) / deltaTime;

    assert(factor >= 0.0f && factor <= 1.0f);

    const aiVector3D& start = nodeAnimation->mPositionKeys[positionIndex].mValue;
    const aiVector3D& end   = nodeAnimation->mPositionKeys[nextPositionIndex].mValue;
    aiVector3D delta = end - start;
    out = start + factor * delta;

}


void SkinnedMesh::setBoneTransform(Shader& shader, const GLfloat currentTime)
{
    std::vector<glm::mat4> transforms;
    getBoneTransforms((float)currentTime, transforms);
    glUniformMatrix4fv(glGetUniformLocation(shader.ID, "bones"), (GLsizei)transforms.size(), GL_FALSE, glm::value_ptr(transforms[0]));
}

void SkinnedMesh::populateBuffers() 
{
    // Buffer de vertices
    glBindBuffer(GL_ARRAY_BUFFER, buffers[POSITION_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(POSITION_LOCATION);
    glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);
    
    glBindBuffer(GL_ARRAY_BUFFER, buffers[NORMAL_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * normals.size(), &normals[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(NORMAL_LOCATION);
    glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[TEXCOORD_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
    glEnableVertexAttribArray(TEX_COORD_LOCATION);
    glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

    glBindBuffer(GL_ARRAY_BUFFER, buffers[BONE_VERTEX_BUFFER]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(bones[0]) * bones.size(), &bones[0], GL_STATIC_DRAW);
    
	glEnableVertexAttribArray(BONE_ID_LOCATION);
	glVertexAttribIPointer(BONE_ID_LOCATION, MAX_NUM_BONES_PER_VERTEX, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);
	glEnableVertexAttribArray(BONE_WEIGHT_LOCATION);
	glVertexAttribPointer(BONE_WEIGHT_LOCATION, MAX_NUM_BONES_PER_VERTEX, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)offsetof(VertexBoneData, weights));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[INDEX_BUFFER]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

}


void SkinnedMesh::render()
{
    glBindVertexArray(VAO);
    
    for (uint i = 0; i < meshes.size(); i++)
    {
        uint materialIndex = meshes[i].materialIndex;
        assert(materialIndex < materials.size());

        if(materials[materialIndex].diffuse) {
            materials[materialIndex].diffuse->bind(GL_TEXTURE0);
        }

        if(materials[materialIndex].specular) {
            materials[materialIndex].specular->bind(GL_TEXTURE1);
        }

        glDrawElementsBaseVertex(
            GL_TRIANGLES, 
            meshes[i].numIndices, 
            GL_UNSIGNED_INT,
            (void*)(sizeof(uint) * meshes[i].baseIndex),
            meshes[i].baseVertex
        );

    }
    
    glBindVertexArray(0);
}
