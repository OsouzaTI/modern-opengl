#include "SimpleModel.h"

SimpleModel::SimpleModel(std::string path, std::string texturePath)
    : Model()
{
    loadObj(path);
    processMesh();

    // por ultimo carregar a textura
    loadTexture(texturePath);
}

SimpleModel::~SimpleModel()
{

}

void SimpleModel::render(Shader& shader) {

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);

    GLuint textureSamplerLocation = glGetUniformLocation(shader.ID, "TextureSampler");
    glUniform1i(textureSamplerLocation, 0);

    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    glBindTexture(GL_TEXTURE_2D, 0);

}

void SimpleModel::processMesh() {

    // gerando o Vertex Array Object
    glGenVertexArrays(1, &VAO);
    // gerando o Vertex Buffer Object
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), &indices[0], GL_STATIC_DRAW);

    // atributo de vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    // atributo de normais
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));

    // atributo de textura
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texture));

    glBindVertexArray(0);

}

void SimpleModel::loadTexture(std::string path) {

    glGenTextures(1, &textureId);

    int width, height, components;
    unsigned char *data = stbi_load(path.c_str(), &width, &height, &components, 0);
    if (data) {
        GLenum format;
        if (components == 1) {
            format = GL_RED;
        } else if (components == 3) {
            format = GL_RGB;
        } else if (components == 4) {
            format = GL_RGBA;
        }
        glBindTexture(GL_TEXTURE_2D, textureId);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);

        // filtros de magnificação e minificação
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        
        // configurações de wraps
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        
        // gerando o mipmap da textura
        glGenerateMipmap(GL_TEXTURE_2D);

        // resetando a textura 
        glBindTexture(GL_TEXTURE_2D, 0);
    } else {
        std::cout << "Texture failed to load at path: " << path << std::endl;
    }

    stbi_image_free(data);

}

void SimpleModel::loadObj(std::string path) {

    std::vector<GLuint> vertexIndices, uvIndices, normalIndices;
    std::vector<glm::vec3> _vertices;
    std::vector<glm::vec2> _uvs;
    std::vector<glm::vec3> _normals;    

    FILE* file = fopen(path.c_str(), "r");
    if(file == NULL) {
        std::cout << "Impossivel abrir arquivo " << path << std::endl;
    } else {

        while(1) {
            char buffer[128];
            int line = fscanf(file, "%s", buffer);
            if(line == EOF) break;

            if(strcmp(buffer, "v") == 0) {
                glm::vec3 v;
                fscanf(file, "%f %f %f\n", &v.x, &v.y, &v.z);
                _vertices.push_back(v);

                // determinando pontos maximos e minimos do objeto
                minMaxPoints.analyze(v);

            } else if(strcmp(buffer, "vt") == 0) {
                glm::vec2 uv;
                fscanf(file, "%f %f\n", &uv.x, &uv.y);
                _uvs.push_back(uv);
            } else if(strcmp(buffer, "vn") == 0) {
                glm::vec3 n;
                fscanf(file, "%f %f %f\n", &n.x, &n.y, &n.z);
                _normals.push_back(n);
            } else if(strcmp(buffer, "f") == 0) {
                GLuint vi[3], uv[3], ni[3];
                int matches = fscanf(file, "%d/%d/%d %d/%d/%d %d/%d/%d\n", 
                    &vi[0], &uv[0], &ni[0],
                    &vi[1], &uv[1], &ni[1],
                    &vi[2], &uv[2], &ni[2]);

                vertexIndices.push_back(vi[0]);
                vertexIndices.push_back(vi[1]);
                vertexIndices.push_back(vi[2]);
                
                uvIndices.push_back(uv[0]);
                uvIndices.push_back(uv[1]);
                uvIndices.push_back(uv[2]);

                normalIndices.push_back(ni[0]);
                normalIndices.push_back(ni[1]);
                normalIndices.push_back(ni[2]);
                
            }

        }

        Vertex vertice;
        for(GLuint i = 0; i < vertexIndices.size(); i++ ) {
            
            GLuint vertexIndex  = vertexIndices[i];            
            GLuint normalIndex  = normalIndices[i];
            GLuint uvIndex      = uvIndices[i];

            // vertice
            glm::vec3 vertex = _vertices[vertexIndex - 1];
            // normal
            glm::vec3 normal = _normals[normalIndex - 1];
            // uv
            glm::vec2 uv = _uvs[uvIndex - 1];

            vertice.position = vertex;
            vertice.normal = normal;
            vertice.texture = uv;
            vertices.push_back(vertice);
            indices.push_back(i);

        }


    }

}