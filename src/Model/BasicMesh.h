#ifndef BASIC_MESH_H
#define BASIC_MESH_H

#include <iostream>
#include <map>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>
#include <Core/Shader.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <Model/BasicBone.h>
#include <Structures/Material.h>
#include <Structures/Texture.h>
#include <Utils/Macros.h>
#include <Utils/EngineCommom.h>
#include <Math/GlmCast.h>

#define POSITION_LOCATION       0
#define NORMAL_LOCATION         1
#define TEX_COORD_LOCATION      2
#define BONE_ID_LOCATION        3
#define BONE_WEIGHT_LOCATION    4

class BasicMesh
{
public:
    BasicMesh();
    ~BasicMesh();

    bool loadMesh(const std::string& file);
    void render();

protected:

    void clear();
   
    struct BasicMeshEntry
    {
        BasicMeshEntry()
        {
            numIndices      =  0;
            baseVertex      =  0;
            baseIndex       =  0;
            materialIndex   = -1;
        }

        uint numIndices;
        uint baseVertex;
        uint baseIndex;
        uint materialIndex;
    };

    std::vector<BasicMeshEntry> meshes;

private:

    const aiScene* scene;
    Assimp::Importer importer;

    enum BUFFER_TYPE {
        POSITION_VERTEX_BUFFER  = 0,
        NORMAL_VERTEX_BUFFER    = 1,
        TEXCOORD_VERTEX_BUFFER  = 2,
        INDEX_BUFFER            = 3,
        // Indica o numero de buffers que serão usados
        NUM_BUFFERS             = 4
    };

    GLuint VAO;
    GLuint buffers[NUM_BUFFERS] = {0};
    
    std::vector<Material> materials;
    
    std::vector<glm::vec3>  positions;
    std::vector<glm::vec3>  normals;
    std::vector<glm::vec2>  texCoords;
    std::vector<uint>       indices;

    void initFromScene(const aiScene* scene, const std::string& file);
    void countVerticesAndIndices(const aiScene* scene, uint& numVertices, uint& numIndices);
    void reserveSpace(uint numVertices, uint numIndices);
    void initAllMeshes(const aiScene* scene);
    void initSingleMesh(uint meshIndex, const aiMesh* mesh);
    void initMaterials(const aiScene* scene, const std::string& file);
    void populateBuffers();

    void loadTextures(const std::string& directory, const aiMaterial* material, int index);

    /**
     * Metodos para o carregamento do material difuso 
     * */
    void loadDiffuseTexture(const std::string& directory, const aiMaterial* material, int materialIndex);
    void loadDiffuseTextureEmbedded(const aiTexture* texture, int materialIndex);
    void loadDiffuseTextureFromFile(const std::string& directory, const aiString& path, int materialIndex);

    /**
     * Metodos para o carregamento do material especular 
     * */
    void LoadSpecularTexture(const std::string& directory, const aiMaterial* material, int materialIndex);
    void LoadSpecularTextureEmbedded(const aiTexture* texture, int materialIndex);
    void LoadSpecularTextureFromFile(const std::string& directory, const aiString& path, int materialIndex);

};





#endif