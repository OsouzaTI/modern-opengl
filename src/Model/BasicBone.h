#ifndef BASIC_BONE_H
#define BASIC_BONE_H

#include <iostream>
#include <assert.h>

#include <map>
#include <string>
#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <Utils/Macros.h>

/**
 * @brief Numero maximo de ossos que 
 * irão influenciar um único vertice
 * da manlha
 */
#define MAX_NUM_BONES_PER_VERTEX 4

#define MAX_BONES 100

struct VertexBoneData
{
    
    VertexBoneData() { };

    /**
     * @brief Identificador de cada osso que
     * exerce uma influencia nesse vertice
     */
    uint boneIds[MAX_NUM_BONES_PER_VERTEX] = {0};
    /**
     * @brief O peso de influencia de cada osso,
     * será utilizado na interpolação das transformações
     */
    float weights[MAX_NUM_BONES_PER_VERTEX] = {0.0f};

    void addBoneData(uint boneId, float weight)
    {
        for (uint i = 0; i < ARRAY_SIZE_IN_ELEMENTS(boneIds); i++)
        {
            if(weights[i] == 0.0f) {
                boneIds[i] = boneId;
                weights[i] = weight;
                // std::cout << "Bone " << boneId << " weight " << weight << " index " << i << std::endl;
                return;
            }
        }
        // assert(0);
    }

};

struct BoneInfo
{
    /**
     * @brief OffsetMatrix é a matriz responsável
     * por o espaço local do modelo para o espaço
     * do osso.
     */
    glm::mat4 offsetMatrix;
    glm::mat4 finalTransformation;
    
    BoneInfo(const glm::mat4& offset) {
        offsetMatrix = offset;
        finalTransformation = glm::mat4(0.0f);
    }

};

#endif