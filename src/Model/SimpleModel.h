#ifndef SIMPLE_MODEL_H
#define SIMPLE_MODEL_H

#include <GL/glew.h>
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <stb/stb_image.h>
#include <Core/Shader.h>
#include <Structures/Vertex.hpp>

#include <Components/BoxCollider.h>
#include <Model/Model.h>

class SimpleModel : public Model
{
private:
    // informações da malha
    unsigned int VAO, VBO, EBO;

    GLuint textureId;
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    void loadObj(std::string path);
    void loadTexture(std::string path);
    void processMesh();

public:

    SimpleModel(std::string path, std::string texturePath);
    ~SimpleModel();
    
    void render(Shader& shader);

};

#endif