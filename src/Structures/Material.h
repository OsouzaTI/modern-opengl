#ifndef MATERIAL_H
#define MATERIAL_H

#include <glm/glm.hpp>
#include <Structures/Texture.h>

struct PBRMaterial
{
    float roughness = 0.0f;
    bool isMetal = false;
    glm::vec3 color{0.0f};
};

class Material
{
public:
    Material();
    ~Material();

    glm::vec3 ambientColor{0.0f};
    glm::vec3 diffuseColor{0.0f};
    glm::vec3 specularColor{0.0f};

    PBRMaterial pbrMaterial;

    Texture* diffuse = nullptr;
    Texture* specular = nullptr;

private:
    
};

#endif