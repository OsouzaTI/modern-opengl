#ifndef VERTEX_H
#define VERTEX_H

#include <glm/glm.hpp>

struct Vertex
{
    glm::vec3 position; // x y z
    glm::vec3 normal;   // x y z
    glm::vec2 texture;  // u v
    glm::vec3 tangent;  // tangent
    glm::vec3 bitangent;// bitangent
};


#endif