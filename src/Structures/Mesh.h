#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <glm/glm.hpp>
#include <vector>

#include <Core/Shader.h>
#include <Structures/Vertex.hpp>
#include <Structures/Texture.h>

struct SimpleTexture {
    unsigned int id;
    std::string type;
    std::string path;
};

class Mesh {
private:
    unsigned int VAO, VBO, EBO;

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<SimpleTexture> textures;

    void processMesh();
public:
    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<SimpleTexture> textures);
    ~Mesh(){};

    void render(Shader &shader);

    std::vector<Vertex> getVertices() const;
    std::vector<unsigned int> getIndices() const;

};

#endif