#include <Structures/Texture.h>

Texture::Texture(GLenum textureTarget, const std::string& filePath)
{
    this->textureTarget = textureTarget;
    this->filePath = filePath;
}

Texture::Texture(GLenum textureTarget)
{
    this->textureTarget = textureTarget;
}

Texture::~Texture()
{
}

bool Texture::load()
{
    stbi_set_flip_vertically_on_load(false);
    unsigned char* imageData = stbi_load(filePath.c_str(), &imageWidth, &imageHeigth, &imageBPP, 0);
    if(!imageData)
    {
        std::cout << "Can't load texture from " << filePath << " " << stbi_failure_reason() << std::endl;
        return false;
    }

    loadInternal(imageData);
    stbi_image_free(imageData);
    return true;
}

bool Texture::loadFromTextures()
{
    std::string finalFilePath = ResourceManager::getTexturePath().append('/' + filePath);
    this->filePath = finalFilePath;
    return load();
}

void Texture::load(uint bufferSize, void* data)
{
    void* imageData = stbi_load_from_memory((const stbi_uc*)data, bufferSize, &imageWidth, &imageHeigth, &imageBPP, 0);
    loadInternal(imageData);
    stbi_image_free(imageData);
}

void Texture::load(const std::string& filePath)
{
    this->filePath = filePath;
    load();
}

void Texture::loadRaw(int width, int height, int BPP, unsigned char* data)
{
    this->imageWidth = width;
    this->imageHeigth = height;
    this->imageBPP = BPP;
    loadInternal(data);
}

void Texture::bind(GLenum textureUnit)
{
    glActiveTexture(textureUnit);
    glBindTexture(textureTarget, textureObj);
}

void Texture::getImageSize(int& imageWidth, int& imageHeight)
{
    imageWidth = this->imageWidth;
    imageHeight = this->imageHeigth;
}

// -------------- PRIVATE METHODS

// TODO: alterar o textureObj para textureId
void Texture::loadInternal(void* imageData)
{

    glGenTextures(1, &textureObj);
    glBindTexture(textureTarget, textureObj);

    if(textureTarget == GL_TEXTURE_2D)
    {
        switch (imageBPP) {
            case 1:
                glTexImage2D(textureTarget, 0, GL_RED, imageWidth, imageHeigth, 0, GL_RED, GL_UNSIGNED_BYTE, imageData);
                break;
            case 3:
                glTexImage2D(textureTarget, 0, GL_RGB, imageWidth, imageHeigth, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
                break;
            case 4:
                glTexImage2D(textureTarget, 0, GL_RGBA, imageWidth, imageHeigth, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
                break;
            default: break;
        }
    }

    glTexParameterf(textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(textureTarget, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(textureTarget, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(textureTarget, 0);

}