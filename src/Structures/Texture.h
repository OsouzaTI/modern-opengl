#ifndef TEXTURE_H
#define TEXTURE_H

#include <iostream>
#include <string>
#include <GL/glew.h>
#include <stb/stb_image.h>
#include <Core/ResourceManager.h>

class Texture
{
public:
    Texture(GLenum textureTarget, const std::string& filePath);
    Texture(GLenum textureTarget);
    ~Texture();
    bool load();
    bool loadFromTextures();
    void load(uint bufferSize, void* data);
    void load(const std::string& filePath);
    void loadRaw(int width, int height, int BPP, unsigned char* data);
    void bind(GLenum textureUnit);
    void getImageSize(int& imageWidth, int& imageHeight);


private:

    void loadInternal(void* imageData);

    std::string filePath;
    GLenum textureTarget;
    GLuint textureObj;
    int imageWidth = 0;
    int imageHeigth = 0;
    int imageBPP = 0;

};

#endif