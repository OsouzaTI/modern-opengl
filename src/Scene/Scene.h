#ifndef SCENE_H
#define SCENE_H

#include <Components/Transform.h>

class SceneObject
{
protected:
    Transform transform;
    unsigned int textureId;
    unsigned int VAO, VBO;
    virtual void loadData() = 0;
};

#endif