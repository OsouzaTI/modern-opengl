#ifndef SKY_BOX_H
#define SKY_BOX_H

#include <iostream>
#include <stb/stb_image.h>
#include <Scene/Scene.h>
#include <Components/Transform.h>

class SkyBox : public SceneObject
{
private:
    void loadData() override;
public:
    SkyBox();
    ~SkyBox();

    inline Transform& getTransform() { return transform; };

    void loadCubeMap(std::vector<std::string> faces);
    void render();
    void bind();
};

#endif