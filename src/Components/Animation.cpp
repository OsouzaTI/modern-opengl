#include <Components/Animation.h>


Animation::Animation(int framesPerSecond)
    : isPlaying(false), state(ANIMATION_STOP), framesPerSecond(framesPerSecond),
        currentAnimation(EMPTY_ANIMATION)
{
    configureAnimationTimer();
}

Animation::~Animation()
{
}

void Animation::configureAnimationTimer() {
    std::cout << "Configuração iniciada..." << std::endl;
    animationTimer = EngineTimer(1000 / framesPerSecond, [&](){        
        this->setAnimationState(animations[currentAnimation](state));
    });
}

void Animation::setAnimationState(AnimationState state) {
    this->state = state;    
}

void Animation::process() {
    
    switch (state)
    {
        case ANIMATION_START    : setIsPlaying(true); break;
        case ANIMATION_STOP     : setIsPlaying(false); break;
        case ANIMATION_FINISHED : setIsPlaying(false); break;
        case ANIMATION_PROGRESS : setIsPlaying(true); break;
        default: setIsPlaying(false);
    }

};

void Animation::update(float deltaTime) {
    
    /**
     * @brief Durante a atuzalização da animação
     * o timer sera processado
     */
    if(isPlaying) {
        animationTimer.process();
    }
}; 

void Animation::setIsPlaying(bool isPlaying) {
    this->isPlaying = isPlaying;
}

void Animation::setCurrentAnimation(std::string animation) {
    this->currentAnimation = animation;
}

void Animation::addAnimation(std::string name, AnimationCallback animation) {
    this->animations[name] = animation;
}

void Animation::play(std::string animation, bool isPermanent) {
    if(!isPlaying) {
        
        if(!(currentAnimation == animation && isPermanent)) {
            std::cout << "Animação iniciada" << std::endl;
            setCurrentAnimation(animation);
            state = ANIMATION_START;
        }

    }
}

void Animation::stop() {
    if(isPlaying) {
        state = ANIMATION_STOP;
    }
}