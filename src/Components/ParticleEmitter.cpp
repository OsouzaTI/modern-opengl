#include <Components/ParticleEmitter.h>

ParticleEmitter::ParticleEmitter(int particleForm)
{
    std::string cubePath    = std::string("assets/objects/cube/cube.obj");
    std::string spherePath  = std::string("assets/objects/sphere/sphere02.obj");
    this->particleForm.loadMesh(cubePath);
    this->flag = ENTITY_RENDER_FLAG;
}

ParticleEmitter::ParticleEmitter(int numOfParticles, int particleForm)
{
    std::string cubePath    = std::string("assets/objects/cube/cube.obj");
    std::string spherePath  = std::string("assets/objects/sphere/sphere02.obj");
    this->particleForm.loadMesh(cubePath);
    setNumOfParticles(numOfParticles);
    this->flag = ENTITY_RENDER_FLAG;
}

ParticleEmitter::~ParticleEmitter()
{

}

void ParticleEmitter::setNumOfParticles(int numOfParticles)
{
    this->numOfParticles = numOfParticles;
    particles.resize(numOfParticles);
}

void ParticleEmitter::setUpdateCallback(ParticleUpdateCallback updateCallback)
{
    this->updateFunction = updateCallback;
}

void ParticleEmitter::process()
{

}

void ParticleEmitter::update(float deltaTime)
{
    for (uint i = 0; i < numOfParticles; i++)
    {
        if(!particles[i].active) {     
            particles[i].reset();
            continue;
        }
        
        if(particles[i].lifeTime <= 0.0f) {
            particles[i].active = false;
        }

        particles[i].lifeTime -= lifeTimeStep * deltaTime * 0.01f; 
        updateFunction(i, particles[i], deltaTime);
    }
}; 

void ParticleEmitter::render(Shader& shader)
{
    shader.bind();
    for (auto particle : particles)
    {
        shader.setMatrix4("projection", Engine::getProjectionMatrix());
        shader.setMatrix4("view", Engine::getViewMatrix());
        shader.setMatrix4("model", particle.transform.getModelMatrix());
        particleForm.render();
    }
}

void ParticleEmitter::render(glm::mat4 viewMatrix, Shader& shader)
{
    shader.bind();
    for (auto particle : particles)
    {
        shader.setMatrix4("projection", Engine::getProjectionMatrix());
        shader.setMatrix4("view", viewMatrix);
        shader.setMatrix4("model", particle.transform.getModelMatrix());
        shader.setFloat("lifeTime", particle.lifeTime);
        particleForm.render();
    }
}