#ifndef ANIMATION_H
#define ANIMATION_H

#include <map>
#include <iostream>
#include <Timer/Timer.h>
#include <Components/Component.h>
#include <functional>

#define EMPTY_ANIMATION "EMPTY_ANIMATION"

typedef enum {
    ANIMATION_START,
    ANIMATION_STOP,
    ANIMATION_PROGRESS,
    ANIMATION_FINISHED
} AnimationState;

typedef std::function<AnimationState(AnimationState lastState)> AnimationCallback;

class Animation : public EntityComponent
{
private:
    int framesPerSecond;
    bool isPlaying;
    AnimationState state;

    std::string currentAnimation;
    std::map<std::string, AnimationCallback> animations;
    
    EngineTimer animationTimer;

    void configureAnimationTimer();
    void setAnimationState(AnimationState state);
    void setIsPlaying(bool isPlaying);
    void setCurrentAnimation(std::string animation);
public:
    Animation() {};
    Animation(int framesPerSecond);
    ~Animation();

    void process() override;
    void update(float deltaTime) override; 
    
    void addAnimation(std::string name, AnimationCallback animation);
    void play(std::string animation, bool isPermanent = false);
    void stop();

    inline int getFramesPerSecond() { return framesPerSecond; };
};


#endif