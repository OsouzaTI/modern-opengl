#include <Components/Transform.h>

Transform::Transform() 
    : scale(glm::vec3(1, 1, 1)), position(glm::vec3(0)), 
        rotate(glm::vec3(0)), velocity(glm::vec3(0)), direction(glm::vec3(0)), angle(0)
{}

Transform::Transform(glm::vec3 position) 
    : scale(glm::vec3(1, 1, 1)), position(position), 
        rotate(glm::vec3(0)), velocity(glm::vec3(0)), direction(glm::vec3(0)), angle(0)
{}

Transform::~Transform() {}

void Transform::update(float deltaTime) {
    position += (velocity * direction) * deltaTime;
}

void Transform::pushMatrix() {
    setIdentityMatrix();
    modelMatrix = glm::translate(modelMatrix, position);
    if(angle != 0) {
        modelMatrix = glm::rotate(modelMatrix, angle, rotate);
    }
    modelMatrix = glm::scale(modelMatrix, scale);
}

glm::mat4 Transform::getModelMatrix() {
    return modelMatrix;
}

glm::mat4 Transform::getModelMatrix(float scaleOffset) {
    return glm::scale(modelMatrix, scale + scaleOffset);
}

// GETTERS AND SETTERS

void Transform::setAngle(float angle) {
    this->angle = angle;
}

void Transform::setPosition(glm::vec3 position) {
    this->position = position;
    // aplicando na matrix
    modelMatrix = glm::translate(modelMatrix, position);
}

void Transform::setScale(glm::vec3 scale) {
    this->scale = scale;
    modelMatrix = glm::scale(modelMatrix, scale);
}

void Transform::setVelocity(glm::vec3 velocity) {
    this->velocity = velocity;
}

void Transform::setDirection(glm::vec3 direction) {
    this->direction = direction;
}

void Transform::setRotate(float angle, glm::vec3 rotate) {
    this->angle = angle;
    this->rotate = rotate;
    // aplicando na matrix
    modelMatrix = glm::rotate(modelMatrix, angle, rotate);
}

void Transform::setRotate(btQuaternion quaternion) {
    this->angle = quaternion.getAngle();
    this->rotate = bt3glm(quaternion.getAxis());
    // aplicando na matrix
    modelMatrix = glm::rotate(modelMatrix, angle, rotate);
}


void Transform::setMass(float mass) {
    this->mass = mass;
}

void Transform::setPositionX(float posx) {
    this->position.x = posx;
    modelMatrix = glm::translate(modelMatrix, position);
}

void Transform::setPositionY(float posy) {
    this->position.y = posy;
    modelMatrix = glm::translate(modelMatrix, position);
}

void Transform::setPositionZ(float posz) {
    this->position.z = posz;
    modelMatrix = glm::translate(modelMatrix, position);
}

void Transform::setIdentityMatrix() {    
    modelMatrix = glm::mat4(1.0f);
}

void Transform::setModelMatrix(glm::mat4 model) {
    modelMatrix = model;
}

// GETTERS

glm::vec3 Transform::getPosition() {
    return position;
}

glm::vec3 Transform::getScale() {
    return scale;
}

glm::vec3 Transform::getVelocity() {
    return velocity;
}

float Transform::getMass() {
    return mass;
}