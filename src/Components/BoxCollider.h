#ifndef BOX_COLLISION_HPP
#define BOX_COLLISION_HPP

#include <iostream>
#include <glm/glm.hpp>
#include <Core/Shader.h>
#include <Components/Collider.h>
#include <Components/Transform.h>
#include <Core/Engine.h>

class AABBCollision3D : public Collider
{       
    // atributos
    unsigned int VAO, VBO, EBO;

    float min_x, min_y, min_z;
    float max_x, max_y, max_z;
    
public:
    AABBCollision3D() : min_x(0), min_y(0), min_z(0), max_x(0), max_y(0), max_z(0) {};
    AABBCollision3D(MinMaxPoints& minMaxPoints, Transform* transform) 
        : min_x(0), min_y(0), min_z(0), max_x(0), max_y(0), max_z(0)
    {
        loadMinMaxValues(minMaxPoints);
        this->parent = transform;
    };
    
    void loadMinMaxValues(MinMaxPoints& minMaxPoints);
    void createVertexBuffer();

    // override collider class
    void analyze(glm::vec3 u) override;
    bool isColliding(glm::vec3 point) override;
    void render(Shader& shader) override;
    void render(glm::mat4 viewMatix, Shader& shader) override;

    // override engine object class
    void process() override {};
    void update(float deltaTime) override {};

};

#endif