#ifndef COLLIDER_H
#define COLLIDER_H

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Core/EngineRenderInterface.h>
#include <Components/Component.h>
#include <Components/Transform.h>
#include <memory>

enum ColliderType {
    BOX_COLLIDER,
};

struct MinMaxPoints
{
    float min_x, max_x;
    float min_y, max_y;
    float min_z, max_z;

    void analyze(glm::vec3 u) {
        
        if(u.x < min_x) {
            min_x = u.x;
        } else if(u.x > max_x) {
            max_x = u.x;
        }

        if(u.y < min_y) {
            min_y = u.y;
        } else if(u.y > max_y) {
            max_y = u.y;
        }

        if(u.z < min_z) {
            min_z = u.z;
        } else if(u.z > max_z) {
            max_z = u.z;
        }

    }

};

class Collider : public EntityComponent, EngineRenderInterface
{

protected:
    Transform* parent;
    float scaleOffset = .1f;
public:

    virtual bool isColliding(glm::vec3 point) = 0;
    virtual void analyze(glm::vec3 u) = 0;

};

#endif