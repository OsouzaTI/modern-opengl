#ifndef TRANSFORM_H
#define TRANSFORM_H
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <Components/Component.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <Math/GlmCast.h>

class Transform : public EntityComponent
{
private:

    // temporary model matrix
    glm::mat4 modelMatrix;

    glm::vec3 scale;
    glm::vec3 position;
    glm::vec3 velocity;
    glm::vec3 rotate;
    glm::vec3 direction;

    float mass;
    float angle;
public:
    Transform();
    Transform(glm::vec3 position);
    ~Transform();

    void setAngle(float angle);
    void setPosition(glm::vec3 position);
    void setScale(glm::vec3 scale);
    void setVelocity(glm::vec3 velocity);
    void setDirection(glm::vec3 direction);
    
    void setRotate(float angle, glm::vec3 rotate);
    void setRotate(btQuaternion quaternion);

    void setMass(float mass);

    void setPositionX(float posx);
    void setPositionY(float posy);
    void setPositionZ(float posz);

    void setIdentityMatrix();
    void setModelMatrix(glm::mat4 model);

    /**
     * @brief Gera a matriz de modelo com base
     * nos atributos ja previamente definidos
     */
    void pushMatrix();

    glm::vec3 getPosition();
    glm::vec3 getScale();
    glm::vec3 getVelocity();
    float getMass();

    glm::mat4 getModelMatrix();
    glm::mat4 getModelMatrix(float scaleOffset);

    void process() override {};
    void update(float deltaTime) override;

};

#endif