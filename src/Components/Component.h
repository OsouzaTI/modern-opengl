#ifndef COMPONENT_H
#define COMPONENT_H

#include <Core/EngineObject.h>

class EntityObject;

typedef enum {
    TRANSFORM_COMPONENT,
    COLLIDER_COMPONENT,
    ANIMATION_COMPONENT
} ComponentType;

enum {
    ENTITY_DEFAULT_FLAG,
    ENTITY_RENDER_FLAG
} EntityFlag;

class EntityComponent {
protected:
    uint flag = ENTITY_DEFAULT_FLAG;
public:
    EntityObject* owner;
    virtual void process() = 0;
    virtual void update(float deltaTime) = 0;
    uint getFlag(){return flag;};
};


#endif