#include "BoxCollider.h"

void AABBCollision3D::loadMinMaxValues(MinMaxPoints& minMaxPoints) {
    min_x = minMaxPoints.min_x;
    max_x = minMaxPoints.max_x;
    min_y = minMaxPoints.min_y;
    max_y = minMaxPoints.max_y;
    min_z = minMaxPoints.min_z;
    max_z = minMaxPoints.max_z;
    createVertexBuffer();
}

void AABBCollision3D::analyze(glm::vec3 u) {
    
    if(u.x < min_x) {
        min_x = u.x;
    } else if(u.x > max_x) {
        max_x = u.x;
    }

    if(u.y < min_y) {
        min_y = u.y;
    } else if(u.y > max_y) {
        max_y = u.y;
    }

    if(u.z < min_z) {
        min_z = u.z;
    } else if(u.z > max_z) {
        max_z = u.z;
    }

}

void AABBCollision3D::createVertexBuffer() {
    
    float vertices[] = {
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		 1.0f, 1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f,
		 1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		 1.0f,-1.0f,-1.0f,
		 1.0f, 1.0f,-1.0f,
		 1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		 1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f,-1.0f, 1.0f,
		 1.0f,-1.0f, 1.0f,
		 1.0f, 1.0f, 1.0f,
		 1.0f,-1.0f,-1.0f,
		 1.0f, 1.0f,-1.0f,
		 1.0f,-1.0f,-1.0f,
		 1.0f, 1.0f, 1.0f,
		 1.0f,-1.0f, 1.0f,
		 1.0f, 1.0f, 1.0f,
		 1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f,-1.0f,
		 1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f,-1.0f,
		-1.0f, 1.0f, 1.0f,
		 1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		 1.0f,-1.0f, 1.0f
    };

    unsigned int indices[] = {
        5, 1, 4,
        5, 4, 8,
        3, 7, 8,
        3, 8, 4,
        2, 6, 3,
        6, 7, 3,
        1, 5, 2,
        5, 6, 2,
        5, 8, 6,
        8, 7, 6,
        1, 2, 3,
        1, 3, 4
    };

    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    // glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    // glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    // atributo de vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

    glBindVertexArray(0);

}

bool AABBCollision3D::isColliding(glm::vec3 point) {
    
    glm::vec3 tfScale = parent->getScale();
    glm::vec3 tfPosition = parent->getPosition();

    if(glm::distance(point, tfPosition) > glm::length(tfScale)) {
        return false;
    }

    glm::vec3 minx = tfPosition + glm::vec3(min_x, 0, 0) * tfScale;
    glm::vec3 maxx = tfPosition + glm::vec3(max_x, 0, 0) * tfScale;
    glm::vec3 miny = tfPosition + glm::vec3(0, min_y, 0) * tfScale;
    glm::vec3 maxy = tfPosition + glm::vec3(0, max_y, 0) * tfScale;
    glm::vec3 minz = tfPosition + glm::vec3(0, 0, min_z) * tfScale;
    glm::vec3 maxz = tfPosition + glm::vec3(0, 0, max_z) * tfScale;
    
    return 
        // colidindo em x
        (point.x >= minx.x && point.x <= maxx.x) &&
        // colidindo em y
        (point.y >= miny.y && point.y <= maxy.y) &&
        // colidindo em z
        (point.z >= minz.z && point.z <= maxz.z);
}

void AABBCollision3D::render(Shader& shader) {

    shader.bind();

    shader.setMatrix4("projection", Engine::getProjectionMatrix());        
    shader.setMatrix4("view", Engine::getViewMatrix());  
    shader.setMatrix4("model", parent->getModelMatrix(scaleOffset));

    // wireframe
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glBindVertexArray(VAO);    
    glDrawArrays(GL_TRIANGLES, 0, 36);    
    glBindVertexArray(0);

    // normal rendering
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

}

void AABBCollision3D::render(glm::mat4 viewMatix, Shader& shader) {
    shader.bind();

    shader.setMatrix4("projection", Engine::getProjectionMatrix());        
    shader.setMatrix4("view", viewMatix);
    shader.setMatrix4("model", parent->getModelMatrix(scaleOffset));

    // wireframe
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    
    glBindVertexArray(VAO);    
    glDrawArrays(GL_TRIANGLES, 0, 36);    
    glBindVertexArray(0);

    // normal rendering
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}