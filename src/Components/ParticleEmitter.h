#ifndef PARTICLE_EMITTER_H
#define PARTICLE_EMITTER_H

#include <glm/glm.hpp>
#include <Core/Engine.h>
#include <Core/EngineRenderInterface.h>
#include <Core/EngineObject.h>
#include <Components/Component.h>
#include <Model/BasicMesh.h>
#include <Timer/Timer.h>
#include <Components/Transform.h>
#include <functional>

enum
{
    PARTICLE_CUBE,
    PARTICLE_SPHERE
};

struct Particle {
    bool active = true;
    float lifeTime = 1.0f;
    Transform transform;
    Particle() : active(true), lifeTime(1.0f), transform(Transform({0, 0, 0})) {};

    void randomSize() {
        float number = ((rand()+1)%15)/10.0f;
        transform.setScale(glm::vec3{number});
    }

    void reset() {
        active = true;
        lifeTime = 1.0f;
    }

    void kill() {
        active = false;
        lifeTime = 0.0f;
    }

};

typedef std::function<void(int index, Particle& particle, float deltaTime)> ParticleUpdateCallback;

class ParticleEmitter : public EntityComponent, EngineRenderInterface
{
private:
    // timer de emissão de particulas
    EngineTimer timer;

    // diretamente ligada ao número de chamadas de renderização
    int numOfParticles;
    float lifeTimeStep = 0.2f;
    // particles
    std::vector<Particle> particles;

    ParticleUpdateCallback updateFunction;

    BasicMesh particleForm;

public:
    ParticleEmitter(int particleForm);
    ParticleEmitter(int numOfParticle, int particleForm);
    ~ParticleEmitter();

    void setNumOfParticles(int numOfParticles);
    void setUpdateCallback(ParticleUpdateCallback updateCallback);

    void process() override;
    void update(float deltaTime) override; 
    void render(Shader& shader) override;

    void render(glm::mat4 viewMatrix, Shader& shader);

};

#endif