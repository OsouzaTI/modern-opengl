#ifndef CAST_VECTOR_H
#define CAST_VECTOR_H

#include <glm/glm.hpp>
#include <bullet/LinearMath/btVector3.h>

#include <assimp/Importer.hpp>

inline btVector3 glm3bt(const glm::vec3& vec)
{
    return { vec.x, vec.y, vec.z };
}

inline glm::vec3 bt3glm(const btVector3& vec)
{
    return { vec.getX(), vec.getY(), vec.getZ() };
}

// For converting between ASSIMP and glm
static inline glm::vec3 vec3_cast(const aiVector3D &v) { return glm::vec3(v.x, v.y, v.z); }
static inline glm::vec2 vec2_cast(const aiVector3D &v) { return glm::vec2(v.x, v.y); }
static inline glm::quat quat_cast(const aiQuaternion &q) { return glm::quat(q.w, q.x, q.y, q.z); }
static inline glm::mat4 mat4_cast(const aiMatrix4x4 &m) { return glm::transpose(glm::make_mat4(&m.a1)); }
static inline glm::mat4 mat4_cast(const aiMatrix3x3 &m) { return glm::transpose(glm::make_mat3(&m.a1)); }

// https://stackoverflow.com/questions/61465738/incorrect-bone-transforms-assimp

#endif