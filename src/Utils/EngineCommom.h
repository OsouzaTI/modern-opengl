#ifndef ENGINE_COMMOM_H
#define ENGINE_COMMOM_H

#include <iostream>
#include <GL/glew.h>

#define ENGINE_ERROR(err) \
    std::cout << "Error::" << err << std::endl

#define ALBEDO_TEXTURE      GL_TEXTURE0
#define NORMAL_TEXTURE      GL_TEXTURE1
#define METALLIC_TEXTURE    GL_TEXTURE2
#define ROUGHNESS_TEXTURE   GL_TEXTURE3
#define AO_TEXTURE          GL_TEXTURE4

#endif