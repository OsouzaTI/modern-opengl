#ifndef SHADER_H
#define SHADER_H

#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <Core/Light.h>
class Shader
{
public:
    // state
    unsigned int ID; 
    // constructor
    Shader() { }
    // sets the current shader as active
    Shader  &bind();
    // compiles the shader from given source code
    void    compile(const char *vertexSource, const char *fragmentSource, const char *geometrySource = nullptr); // note: geometry source code is optional 
    // utility functions
    Shader&    setFloat    (const char *name, float value, bool useShader = false);
    Shader&    setInteger  (const char *name, int value, bool useShader = false);
    Shader&    setVector2f (const char *name, float x, float y, bool useShader = false);
    Shader&    setVector2f (const char *name, const glm::vec2 &value, bool useShader = false);
    Shader&    setVector3f (const char *name, float x, float y, float z, bool useShader = false);
    Shader&    setVector3f (const char *name, const glm::vec3 &value, bool useShader = false);
    Shader&    setVector4f (const char *name, float x, float y, float z, float w, bool useShader = false);
    Shader&    setVector4f (const char *name, const glm::vec4 &value, bool useShader = false);
    Shader&    setMatrix4  (const char *name, const glm::mat4 &matrix, bool useShader = false);

    // ---------- Engine Helper ------------------------------------------ //
    Shader& setDirectionalLight(DirectionalLight& light);

private:    
    void    checkCompileErrors(unsigned int object, std::string type); 
};

#endif