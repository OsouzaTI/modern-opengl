#include "ResourceManager.h"

// Instantiate static variables
std::map<std::string, Texture2D>    ResourceManager::textures;
std::map<std::string, Shader>       ResourceManager::shaders;

std::string  ResourceManager::ASSETS_PATH   = std::string("assets");
std::string  ResourceManager::SHADER_PATH   = std::string("shaders");
std::string  ResourceManager::TEXTURE_PATH  = std::string("textures");

Shader ResourceManager::loadShader(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile, std::string name)
{
    std::string vShaderPath = makePath(SHADER_PATH, std::string(vShaderFile) + ".vert");
    std::string fShaderPath = makePath(SHADER_PATH, std::string(fShaderFile) + ".frag");
    // std::cout << "Vertex Shader: " << vShaderPath << std::endl;
    // std::cout << "Fragment Shader: " << fShaderPath << std::endl;
    shaders[name] = loadShaderFromFile(vShaderPath.c_str(), fShaderPath.c_str(), gShaderFile);
    return shaders[name];
}

Shader ResourceManager::getShader(std::string name)
{
    return shaders[name];
}

Texture2D ResourceManager::loadTexture(const char *file, bool alpha, std::string name)
{
    // std::filesystem::path assets(ASSETS_PATH);
    // std::filesystem::path texture(TEXTURE_PATH);    
    std::string texturePath = std::string(ASSETS_PATH) + "/" + std::string(TEXTURE_PATH);
    std::string filePath = makePath(texturePath, std::string(file));
    std::cout << filePath << std::endl;
    textures[name] = loadTextureFromFile(filePath.c_str(), alpha);
    return textures[name];
}

Texture2D ResourceManager::getTexture(std::string name)
{
    return textures[name];
}

void ResourceManager::clear()
{
    // (properly) delete all shaders	
    for (auto iter : shaders)
        glDeleteProgram(iter.second.ID);
    // (properly) delete all textures
    for (auto iter : textures)
        glDeleteTextures(1, &iter.second.ID);
}

Shader ResourceManager::loadShaderFromFile(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile)
{
    // 1. retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;
    try
    {
        // open files
        std::ifstream vertexShaderFile(vShaderFile);
        std::ifstream fragmentShaderFile(fShaderFile);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vertexShaderFile.rdbuf();
        fShaderStream << fragmentShaderFile.rdbuf();
        // close file handlers
        vertexShaderFile.close();
        fragmentShaderFile.close();
        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
        // if geometry shader path is present, also load a geometry shader
        if (gShaderFile != nullptr)
        {
            std::ifstream geometryShaderFile(gShaderFile);
            std::stringstream gShaderStream;
            gShaderStream << geometryShaderFile.rdbuf();
            geometryShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch (std::exception e)
    {
        std::cout << "ERROR::SHADER: Failed to read shader files" << std::endl;
    }
    const char *vShaderCode = vertexCode.c_str();
    const char *fShaderCode = fragmentCode.c_str();
    const char *gShaderCode = geometryCode.c_str();
    // 2. now create shader object from source code
    Shader shader;
    shader.compile(vShaderCode, fShaderCode, gShaderFile != nullptr ? gShaderCode : nullptr);
    return shader;
}

Texture2D ResourceManager::loadTextureFromFile(const char *file, bool alpha)
{
    // create texture object
    Texture2D texture;
    if (alpha)
    {
        texture.internalFormat = GL_RGBA;
        texture.imageFormat = GL_RGBA;
    }
    // load image
    int width, height, nrChannels;
    unsigned char* data = stbi_load(file, &width, &height, &nrChannels, 0);
    // now generate texture
    texture.generate(width, height, data);
    // and finally free image data
    stbi_image_free(data);
    return texture;
}

std::string ResourceManager::makePath(std::string path, std::string file) {
    // std::filesystem::path directory(path);
    // std::filesystem::path fileName(file);
    return std::string(path) + "/" + std::string(file);
}