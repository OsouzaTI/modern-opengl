#include <Core/Entity.h>
#include <Components/ParticleEmitter.h>

std::shared_ptr<Shader> EntityObject::shaderCollider = nullptr;

EntityObject::EntityObject() : renderCollider(false) {
    initialize();
}

EntityObject::EntityObject(std::string modelPath) : renderCollider(false) {
    initialize();
    loadModel(modelPath);
}

void EntityObject::initialize() {}

EntityObject::~EntityObject() {}

void EntityObject::setRenderCollider(bool renderCollider) {
    this->renderCollider = renderCollider;
}

void EntityObject::loadModel(std::string path) {

    // verificando se o shader de colisores fora carregado
    if(shaderCollider == nullptr) {
        std::cout << "Loading default static shaders..." << std::endl;
        Shader shader = LOAD_VF_SHADER("shader_b", "shader_a", NULL, "collider");
        // carregando o shader estatico
        shaderCollider = std::make_shared<Shader>(shader);
    }

    model3D = std::make_shared<AssimpModel>();
    model3D->loadModel(path);

    addComponent<Transform>(glm::vec3{0, 0, 0});
    addComponent<AABBCollision3D>(model3D->getMinMaxPoints(), getComponent<Transform>());
    
}

void EntityObject::process() {
    for(auto& component: components){
        component->process();
    }
};

void EntityObject::update(float deltaTime) {
    for(auto& component: components){
        component->update(deltaTime);
    }
};

void EntityObject::render(Shader& shader) {
    shader.setMatrix4("projection", Engine::getProjectionMatrix());        
    shader.setMatrix4("view", Engine::getViewMatrix());  
    shader.setMatrix4("model", getComponent<Transform>()->getModelMatrix());
    shader.setFloat("time", static_cast<float>(glfwGetTime()));
    if(model3D != nullptr) {        
        model3D->render(shader);
    }

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    renderComponents(shader);
    
}

void EntityObject::render(glm::mat4 viewMatix, Shader& shader) {
    shader.setMatrix4("projection", Engine::getProjectionMatrix());        
    shader.setMatrix4("view", viewMatix);  
    shader.setMatrix4("model", getComponent<Transform>()->getModelMatrix());
    shader.setFloat("time", static_cast<float>(glfwGetTime()));
    if(model3D != nullptr) {        
        model3D->render(shader);
    }
    
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    renderComponents(viewMatix, shader);
    
}

void EntityObject::renderComponents(Shader& shader) {
    for(auto& component: components) { 
        ParticleEmitter* pe = dynamic_cast<ParticleEmitter*>(component);
        if(pe) {
            std::cout << "Render" << std::endl;
            pe->render(shader);
        }
    }
}

void EntityObject::renderComponents(glm::mat4 viewMatix, Shader& shader) {
    for(auto& component: components) {
        if(typeid(component) == typeid(EngineRenderInterface)) {
            ((EngineRenderInterface&)component).render(viewMatix, shader);
        }
    }
}