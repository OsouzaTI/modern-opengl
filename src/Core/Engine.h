#ifndef ENGINE_H
#define ENGINE_H

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <functional>
#include <Core/Entity.h>
#include <Core/Camera.h>
#include <Math/Helpers.h>

#include <bullet/btBulletDynamicsCommon.h>
#include <Core/ResourceManager.h>

#include <irrklang/irrKlang.h>

#define KEYS_SIZE 1024

enum EngineKeyEventType
{
    ENGINE_KEY_PRESS,
    ENGINE_KEY_RELEASE
};

#define N_ENGINE_AUDIO_CHANNELS 4

enum EngineAudioChannel {
    AUDIO_CHANNEL_0,
    AUDIO_CHANNEL_1,
    AUDIO_CHANNEL_2,
    AUDIO_CHANNEL_3,
};

class EntityObject;

typedef std::function<void(GLFWwindow* window, float deltaTime)> ProcessCallback;
typedef std::function<void(float deltaTime)> UpdateCallback;
typedef std::function<void(float deltaTime)> RenderCallback;

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCallback(GLFWwindow* window, double xpos, double ypos);
void framebufferSizeCallback(GLFWwindow* window, int width, int height);

class Engine
{

private:
    GLFWwindow* window;

    // sistema de audio
    irrklang::ISoundEngine* audio[N_ENGINE_AUDIO_CHANNELS];

    /**
     * @brief Variáveis destinadas a inicialização
     * do motor de física Bullet Physics
     */
    btDefaultCollisionConfiguration*        collisionConfiguration;
    btCollisionDispatcher*                  dispatcher;
    btBroadphaseInterface*                  overlappingPairCache;
    btSequentialImpulseConstraintSolver*    solver;
    btDiscreteDynamicsWorld*                dynamicsWorld;

    void destroyBulletPhysics();

    // delta time
    float deltaTime = 0.0f;
    float lastFrame = 0.0f;

    // inicialize glfw
    bool initGlfw();
    bool initGlew();

    bool debugMode;
    bool hideMouse;

    // configure opengl
    bool configureOpenGL();
    bool configureImGui();
    bool configureStbImage();
    bool configureBulletPhysics();
    bool configureAudioSystems();

    void updateDeltaTime();

    ProcessCallback processFunction;
    UpdateCallback  updateFunction;
    RenderCallback  renderFunction;

    // objetos de cena
    std::vector<EngineObject*> entities;

    static int width, height;
    static bool keys[KEYS_SIZE];
    static glm::vec2 mouse;
    static Camera camera;

public:

    Engine(int width, int height);
    ~Engine();

    static void setEngineSize(int width, int height);
    inline static bool keyPress(unsigned int key) { return keys[key]; };
    inline static void keyUnPress(unsigned int key) { keys[key] = false; };
    inline static float getAspectRatio() { return width/(float)height; };

    inline static int getWidth(){ return width; };
    inline static int getHeight(){ return height; };
    inline static glm::vec2 getMousePosition(){ return mouse; }

    static void setKeyEvent(unsigned int key, EngineKeyEventType type);
    static void setMousePosition(double xpos, double ypos);

    static Camera& getCamera();
    static glm::mat4 getProjectionMatrix();
    static glm::mat4 getViewMatrix();
    static glm::vec3 getCameraPosition();

    btDynamicsWorld* getBtDynamicsWorld();

    static void setCameraPosition(glm::vec3 position);
    static void enableCullFace();
    static void disableCullFace();

    void setProcessCallback(ProcessCallback process);
    void setUpdateCallback(UpdateCallback update);
    void setRenderCallback(RenderCallback render);

    void setDebugMode(bool debug);
    void setHideMouse(bool hide);

    void addEntity(EngineObject* entity);

    // shaders carregados pela engine
    void loadShader(std::string file, std::string name);
    void loadShader(std::string file, std::string geometry, std::string name);
    Shader& getShader(std::string name);
    // texturas carregadas pela engine
    void loadTexture(std::string file, std::string name, bool hasAlpha = false);
    Texture2D& getTexture(std::string name);

    // audio functions

    irrklang::ISound* playAudio2D(EngineAudioChannel channel, std::string filePath, bool loop = false);
    void setAudioVolume(EngineAudioChannel channel, float volume);
    bool audioChannelIsPlaying(EngineAudioChannel channel, irrklang::ISound* sound);

    void process();
    void update();
    void render();

    void run();

};

#endif