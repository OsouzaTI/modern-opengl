#ifndef PBR_TEXTURE_H
#define PBR_TEXTURE_H

#include <iostream>
#include <Structures/Texture.h>
#include <Utils/EngineCommom.h>
#include <memory>

class PBRTexture {

private:

    std::shared_ptr<Texture> albedo;
    std::shared_ptr<Texture> normal;
    std::shared_ptr<Texture> metallic;
    std::shared_ptr<Texture> roughness;
    std::shared_ptr<Texture> ao;



public:

    PBRTexture(std::string albedo, std::string normal, std::string metallic, std::string roughness, std::string ao) {
        this->albedo = std::make_shared<Texture>(GL_TEXTURE_2D, albedo);
        this->normal = std::make_shared<Texture>(GL_TEXTURE_2D, normal);
        this->metallic = std::make_shared<Texture>(GL_TEXTURE_2D, metallic);
        this->roughness = std::make_shared<Texture>(GL_TEXTURE_2D, roughness);
        this->ao = std::make_shared<Texture>(GL_TEXTURE_2D, ao);
    
        if(!this->albedo->loadFromTextures()) {
            ENGINE_ERROR("Erro ao carregar albedo texture");
        }

        if(!this->normal->loadFromTextures()) {
            ENGINE_ERROR("Erro ao carregar normal texture");
        }

        if(!this->metallic->loadFromTextures()) {
            ENGINE_ERROR("Erro ao carregar metallic texture");
        }

        if(!this->roughness->loadFromTextures()) {
            ENGINE_ERROR("Erro ao carregar roughness texture");
        }

        if(!this->ao->loadFromTextures()) {
            ENGINE_ERROR("Erro ao carregar ao texture");
        }

    };

    void bind() {
        albedo->bind(ALBEDO_TEXTURE);
        normal->bind(NORMAL_TEXTURE);
        metallic->bind(METALLIC_TEXTURE);
        roughness->bind(ROUGHNESS_TEXTURE);
        ao->bind(AO_TEXTURE);
    }

};

#endif