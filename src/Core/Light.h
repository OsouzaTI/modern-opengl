#ifndef LIGHT_H
#define LIGHT_H

#include <glm/glm.hpp>

struct DirectionalLight
{
    float intensity;
    glm::vec3 direction;
    glm::vec3 color;
};


#endif