#include <Core/Engine.h>

void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
   
    if(action == GLFW_PRESS) {
        Engine::setKeyEvent(key, ENGINE_KEY_PRESS);
    }

    if(action == GLFW_RELEASE) {
        Engine::setKeyEvent(key, ENGINE_KEY_RELEASE);
    }

}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    Engine::getCamera().updateMouse(xpos, ypos);
    Engine::setMousePosition(xpos, ypos);
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
    Engine::setEngineSize(width, height);
}

// variaveis estaticas
int Engine::width = 0;
int Engine::height = 0;
bool Engine::keys[KEYS_SIZE];
Camera Engine::camera;
glm::vec2 Engine::mouse{0.0f};

Engine::Engine(int width, int height) : debugMode(false), hideMouse(false)
{
    Engine::setEngineSize(width, height);
    initGlfw();
    initGlew();
    configureOpenGL();
    configureStbImage();
    configureImGui();
    configureBulletPhysics();
    configureAudioSystems();
}

Engine::~Engine()
{
    destroyBulletPhysics();
}

bool Engine::initGlfw() {
    
    if (!glfwInit()) {
        return EXIT_FAILURE;
    }

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(width, height, "Hello World", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return EXIT_FAILURE;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, keyboardCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    
    return true;
}

bool Engine::initGlew() {
    
    if(glewInit() != GLEW_OK) {
        return EXIT_FAILURE;
    }
    
    return true;

}

bool Engine::configureOpenGL() {
    
    // habilitando o teste de profundidade (Z-buffer)
    glEnable(GL_DEPTH_TEST);
    // habilitando o backface culling (econdendo faces internas)
    glEnable(GL_CULL_FACE);
    // face interna 
    glCullFace(GL_BACK);
    // cor de limpeza do buffer
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

    return true;
}

bool Engine::configureImGui() {

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330");

    return true;

}

bool Engine::configureStbImage() {
    // flip nas imagens carregadas pelo stb
    stbi_set_flip_vertically_on_load(true);
    return true;
}

bool Engine::configureBulletPhysics()
{
    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);
    overlappingPairCache = new btDbvtBroadphase();
    solver = new btSequentialImpulseConstraintSolver();
    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);

    return true;
}

bool Engine::configureAudioSystems() {
    for (uint channel = 0; channel < N_ENGINE_AUDIO_CHANNELS; channel++)
    {
        audio[channel] = irrklang::createIrrKlangDevice();
        if(!audio[channel]) {
            return false;
        }    
    }    
    return true;
}

void Engine::destroyBulletPhysics() {

    // remove os rigidBody do mundo e destroi
	for (int i = dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
	{
		btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i];
		btRigidBody* body = btRigidBody::upcast(obj);
		if (body && body->getMotionState())
		{
			delete body->getMotionState();
		}
		dynamicsWorld->removeCollisionObject(obj);
		delete obj;
	}

	//delete dynamics world
	delete dynamicsWorld;
	//delete solver
	delete solver;
	//delete broadphase
	delete overlappingPairCache;
	//delete dispatcher
	delete dispatcher;
    // deletando a configuração de colisao
	delete collisionConfiguration;

}

void Engine::updateDeltaTime() {
    // atualizando delta time
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
}

void Engine::setEngineSize(int width, int height) {
    Engine::width = width;
    Engine::height = height;
}

void Engine::setKeyEvent(unsigned int key, EngineKeyEventType type) {
    switch (type)
    {
        case ENGINE_KEY_PRESS:
            keys[key] = true;
            break;
        case ENGINE_KEY_RELEASE:
            keys[key] = false;
            break;
        default:
            keys[key] = false;
            break;
    }
}

void Engine::setMousePosition(double xpos, double ypos) {
    mouse.x = xpos;
    mouse.y = ypos;
}

Camera& Engine::getCamera() {
    return camera;
}

glm::mat4 Engine::getProjectionMatrix() {
    return glm::perspective(glm::radians(camera.getFov()), Engine::getAspectRatio(), 0.1f, 250.0f);
}

glm::mat4 Engine::getViewMatrix() {
    return camera.getViewMatrix();
}

glm::vec3 Engine::getCameraPosition() {
    return camera.getPosition();
}

// --------------- BULLET PHYSICS ---------------//

btDynamicsWorld* Engine::getBtDynamicsWorld() {
    return dynamicsWorld;
}

void Engine::setCameraPosition(glm::vec3 position) {
    camera.setPosition(position);
}

void Engine::enableCullFace() {
    // habilitando o backface culling (econdendo faces internas)
    glEnable(GL_CULL_FACE);
    // face interna 
    glCullFace(GL_BACK);
}

void Engine::disableCullFace() {
    // habilitando o backface culling (econdendo faces internas)
    glDisable(GL_CULL_FACE);
}

// Audio Functions

irrklang::ISound* Engine::playAudio2D(EngineAudioChannel channel, std::string filePath, bool loop) {
    return audio[channel]->play2D(filePath.c_str(), loop);
}

void Engine::setAudioVolume(EngineAudioChannel channel, float volume) {
    audio[channel]->setSoundVolume(volume >= 0.0f && volume <= 1.0f ? volume : 1.0f);
}

bool Engine::audioChannelIsPlaying(EngineAudioChannel channel, irrklang::ISound* sound) {
    if(sound == NULL) {
        return false;
    }
    return audio[channel]->isCurrentlyPlaying(sound->getSoundSource());
}

void Engine::process() {
    for (auto& entity : entities)
    {
        entity->process();
    }
    processFunction(window, deltaTime);
}

void Engine::update() {
    updateDeltaTime();
    
    // atualizando o mundo físico de simulação
    dynamicsWorld->stepSimulation(deltaTime);

    for (auto& entity : entities)
    {
        entity->update(deltaTime);
    }
    
    updateFunction(deltaTime);
}

void Engine::render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    renderFunction(deltaTime);

    // If debug mode is enabled
    if(debugMode)
    {
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    }

}

void Engine::setProcessCallback(ProcessCallback process) {
    processFunction = process;
}

void Engine::setUpdateCallback(UpdateCallback update) {
    updateFunction = update;
}

void Engine::setRenderCallback(RenderCallback render) {
    renderFunction = render;
}

void Engine::setDebugMode(bool debug) {
    this->debugMode = debug;
}

void Engine::setHideMouse(bool hide) {
    this->hideMouse = hide;
    if(hide) {
        // desabilitando o mouse
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);        
    } else {
        // desabilitando o mouse
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);        
    }
}

void Engine::addEntity(EngineObject* entity) {
    entities.push_back(entity);
}

void Engine::loadShader(std::string file, std::string name)
{
    LOAD_SHADER(file.c_str(), NULL, name.c_str());
}

void Engine::loadShader(std::string file, std::string geometry, std::string name)
{
    LOAD_SHADER(file.c_str(), geometry.c_str(), name.c_str());
}

Shader& Engine::getShader(std::string name)
{
    return ResourceManager::shaders[name];
}

void Engine::loadTexture(std::string file, std::string name, bool hasAlpha)
{
    LOAD_TEXTURE(file.c_str(), hasAlpha, name.c_str());
}

Texture2D& Engine::getTexture(std::string name)
{
    return ResourceManager::textures[name];
}

void Engine::run() {

    while (!glfwWindowShouldClose(window))
    {        
        glfwPollEvents();

        // process function
        process();
        // update function
        update();
        // render function
        render();

        glfwSwapBuffers(window);        
    }

    glfwTerminate();

}
