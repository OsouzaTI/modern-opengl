#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H

#include <GL/glew.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <filesystem>
#include <string>
#include <map>
#include <stb/stb_image.h>
#include "Shader.h"
#include "Texture2D.h"

class ResourceManager
{
public:
    static std::string ASSETS_PATH;
    static std::string SHADER_PATH;
    static std::string TEXTURE_PATH;
    
    static std::map<std::string, Shader>    shaders;
    static std::map<std::string, Texture2D> textures;
    static Shader    loadShader(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile, std::string name);
    static Shader    getShader(std::string name);
    static Texture2D loadTexture(const char *file, bool alpha, std::string name);
    static Texture2D getTexture(std::string name);
    inline static std::string getTexturePath() { return ASSETS_PATH + '/' + TEXTURE_PATH;  };    
    static void      clear();
private:
    ResourceManager() { }    
    static Shader    loadShaderFromFile(const char *vShaderFile, const char *fShaderFile, const char *gShaderFile = nullptr);    
    static Texture2D loadTextureFromFile(const char *file, bool alpha);
    static std::string makePath(std::string path, std::string file);
};

#define LOAD_TEXTURE(file, alpha, name) ResourceManager::loadTexture(file, alpha, name)
#define GET_TEXTURE(name) ResourceManager::getTexture(name)
#define LOAD_SHADER(shader, geometry, name) ResourceManager::loadShader(shader, shader, geometry, name)
#define GET_SHADER(name) ResourceManager::getShader(name)
#define LOAD_VF_SHADER(vShader, fShader, geometry, name) ResourceManager::loadShader(vShader, fShader, geometry, name)

#endif