#ifndef ENGINE_OBJECT_H
#define ENGINE_OBJECT_H

#include <Core/EngineRenderInterface.h>

class EngineObject : public EngineRenderInterface
{   
public:
    virtual void process() = 0;
    virtual void update(float deltaTime) = 0;

};

#endif