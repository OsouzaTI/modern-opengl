#ifndef OBJECT_HPP
#define OBJECT_HPP

#include <GL/glew.h>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "Vertex.hpp"
#include <functional>

class Object
{
protected:

    /**
     * @brief id da textura usada pelo objeto
     * 
     */
    unsigned int textureId;

    float scl;
    float angle;
    glm::vec3 position;
    glm::vec3 axisRotation;
    // opengl buffers
    unsigned int VAO, VBO, EBO;
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    
    /***
     * @brief metodo responsavel por processar o objeto e
     * criar os buffers do opengl
     * */
    virtual void process() = 0;

public:
    Object();
    ~Object();

    virtual void render() = 0;

    void translate(glm::vec3 position);
    void scale(float scale);
    void rotate(float angle, glm::vec3 axis);

    glm::mat4 getModelMatrix() const;
    void bind() const;

    void setTextureId(unsigned int textureId);

    glm::vec3 getObjectPosition() const;
    float getObjectScale() const;
    void reset();

    virtual bool isPointInside(glm::vec3 point) const = 0;
};

Object::Object() : position(glm::vec3(0)), axisRotation(glm::vec3(0)), textureId(-1), angle(0)
{
}

Object::~Object()
{
}


void Object::translate(glm::vec3 position) {
    this->position = position;
}

void Object::scale(float scale) {
    this->scl = scale;
}

void Object::rotate(float angle, glm::vec3 axis) {
    this->angle = angle;
    this->axisRotation = axis;
}

void Object::bind() const {
    glBindVertexArray(VAO);
}

void Object::reset() {
    this->scl = 1.0f;
    this->angle = 0.0f;
    this->position = glm::vec3(0);
    this->axisRotation = glm::vec3(0);
}

glm::vec3 Object::getObjectPosition() const {
    return this->position;
}

float Object::getObjectScale() const {
    return this->scl;
}

void Object::setTextureId(unsigned int textureId) {
    this->textureId = textureId;
}

glm::mat4 Object::getModelMatrix() const {
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, this->position);
    model = glm::scale(model, glm::vec3(scl, scl, scl));
    if(angle != 0) {
        model = glm::rotate(model, angle, axisRotation);
    }
    return model;
}

#endif