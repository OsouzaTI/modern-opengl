#ifndef ENTITY_H
#define ENTITY_H

#include <memory>
#include <map>
#include <vector>
#include <Core/EngineObject.h>
#include <Components/Component.h>
#include <Components/Transform.h>
#include <Components/Animation.h>
#include <Model/Model.h>
#include <Model/AssimpModel.h>

class Model;

class EntityObject : public EngineObject
{
private:
    
    std::vector<EntityComponent*> components;
    std::map<const std::type_info*, EntityComponent*> componentTypeMap;

    std::shared_ptr<Model>      model3D;
    
    // shader de colider
    static std::shared_ptr<Shader> shaderCollider;

    bool renderCollider;

    void initialize();

public:
    EntityObject();
    EntityObject(std::string modelPath);
    ~EntityObject();

    template <typename T, typename... TArgs>
    T& addComponent(TArgs&&... args){
        T* newComponent(new T(std::forward<TArgs>(args)...));
        newComponent->owner = this;
        components.emplace_back(newComponent);
        componentTypeMap[&typeid(*newComponent)] = newComponent;
        return *newComponent;
    };

    template <typename T>
    T* getComponent(){
        return static_cast<T*>(componentTypeMap[&typeid(T)]);
    };
    
    template <typename T>
    bool hasComponent() const {            
        return componentTypeMap.count(&typeid(T)) == 0 ? false : true;
    };
    
    void loadModel(std::string path);
    void setRenderCollider(bool renderCollider);


    void process() override;
    void update(float deltaTime) override;
    void render(Shader& shader) override;
    void render(glm::mat4 viewMatix, Shader& shader);
    
    /**
     * Alguns componentes herdam da classe EngineRenderInterface
     * o que dá a possibilidade de renderizar algo na pipeline.
     * */
    void renderComponents(Shader& shader);
    void renderComponents(glm::mat4 viewMatix, Shader& shader);
    
    std::shared_ptr<Model> getModel3D();

};

#endif