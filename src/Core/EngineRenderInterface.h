#ifndef ENGINE_RENDER_OBJECT_H
#define ENGINE_RENDER_OBJECT_H

#include <glm/glm.hpp>
#include <Core/Shader.h>

class EngineRenderInterface
{
public:
    virtual void render(Shader& shader) = 0;
    virtual void render(glm::mat4 viewMatix, Shader& shader) = 0;
};

#endif