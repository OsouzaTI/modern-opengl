#include <Core/Camera.h>

Camera::Camera(glm::vec3 position)
    : yaw(-90.0f), pitch(0.0f), speed(2.5f), sensitivity(0.1f), fov(45.0f), 
    orbitalCamera(false),
    // valores padrões 
    position(position), lastPosition(glm::vec3(0)), front(glm::vec3(0, 0, -1)),
    // vetores up
    up(glm::vec3(0, 1, 0)), worldUp(glm::vec3(0, 1, 0))
{
    update();
}

Camera::~Camera()
{
}

void Camera::processKeyboard(CameraDirection direction, float deltaTime) {
    float velocity = speed * deltaTime;
    lastPosition = position;
    switch (direction)
    {
        case FORWARD:   position += front * velocity; break;
        case BACKWARD:  position -= front * velocity; break;
        case LEFT:      position -= right * velocity; break;
        case RIGHT:     position += right * velocity; break;
        default: break;
    }
    
}

void Camera::updateMouse(double xpos, double ypos) {

    glm::vec2 mpos((float)xpos, (float)ypos);
    
    if(firstMouse) {
        lastMousePosition = mpos;
        firstMouse = false;
    }

    glm::vec2 mouseOffset{
        mpos.x - lastMousePosition.x,
        lastMousePosition.y - mpos.y,
    };

    lastMousePosition = mpos;

    // processa o movimento do mouse sobre a camera
    processMouseMovement(mouseOffset.x, mouseOffset.y);

}

void Camera::processMouseMovement(float xoffset, float yoffset) {

    if(orbitalCamera) {

        xoffset *= sensitivity;
        yoffset *= sensitivity;
        yaw += xoffset;
        pitch += yoffset;   

        // PITCH
        if (pitch > 89.0f) {
            pitch = 89.0f;
        } 
        
        if (pitch < -89.0f) {
            pitch = -89.0f;
        }

        update();

    }
    
}

void Camera::update() {

    if(orbitalCamera) {

        glm::vec3 nFront = {
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
        };

        // atualizando o vetor de frente
        front = glm::normalize(nFront);
        // recalculando os vetores esquerdo e cima
        right = glm::normalize(glm::cross(front, worldUp));
        up = glm::normalize(glm::cross(right, front));
    
    }

}

glm::mat4 Camera::getViewMatrix() {
    return glm::lookAt(position, position + front, up);
}

glm::vec3 Camera::getPosition() {
    return position;
}

void Camera::lockAxis(glm::vec3 axis) {
    if(axis.x != 0) position.x = axis.x;
    if(axis.y != 0) position.y = axis.y;
    if(axis.z != 0) position.z = axis.z;
}

void Camera::setPosition(glm::vec3 position) {
    this->position = position;
}

void Camera::setFront(glm::vec3 front) {
    this->front = front;
}

void Camera::setSpeed(float speed) {
    this->speed = speed;
}

void Camera::setFov(float fov) {
    this->fov = fov;
}

void Camera::setOrbitalCamera(bool orbital) {
    this->orbitalCamera = orbital;
}

void Camera::setYaw(float yaw) {
    this->yaw = yaw;
}

void Camera::setPitch(float pitch) {
    this->pitch = pitch;
}

float Camera::getYaw() {
    return yaw;
}

float Camera::getPitch() {
    return pitch;
}

float Camera::getFov() {
    return fov;
}

glm::vec3 Camera::getFront() {
    return front;
}

glm::vec3 Camera::getLastPosition() {
    return lastPosition;
}