#ifndef CAMERA_H
#define CAMERA_H

#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/string_cast.hpp>
#include <Math/Helpers.h>

enum CameraDirection {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

class Camera
{
private:
    float yaw;
    float pitch;
    float speed;
    float sensitivity;
    float fov;

    bool orbitalCamera;

    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 up;
    glm::vec3 right;
    glm::vec3 worldUp;

    glm::vec3 lastPosition;

    void update();

    glm::vec2 lastMousePosition;
    bool firstMouse = true;
    void processMouseMovement(float x, float y);

public:
    Camera(glm::vec3 position = glm::vec3(0));
    ~Camera();

    void processKeyboard(CameraDirection direction, float deltaTime);
    void updateMouse(double xpos, double ypos);
    
    glm::mat4 getViewMatrix();
    glm::vec3 getPosition();
    void lockAxis(glm::vec3 axis);
    void lockMouseMove(bool lock);
    
    void setFront(glm::vec3 front);
    void setPosition(glm::vec3 position);
    void setSpeed(float speed);
    void setFov(float fov);
    void setOrbitalCamera(bool orbital);
    void setYaw(float yaw);
    void setPitch(float pitch);

    float getYaw();
    float getPitch();
    float getFov();
    glm::vec3 getFront();


    glm::vec3 getLastPosition();

};

#endif