#ifndef CUBE_H
#define CUBE_H

#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <vector>
#include <Core/Vertex.hpp>
#include <Core/Shader.h>
#include <Core/Object.hpp>

#include <functional>

class Cube : public Object
{

protected:
    void process() override;

public:
    Cube();
    ~Cube();

    void render() override;
    bool isPointInside(glm::vec3 point) const override;
};

Cube::Cube()
{
    process();
}

Cube::~Cube() {}

void Cube::render() {
    glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_INT, 0);
}

bool Cube::isPointInside(glm::vec3 point) const {

    float offset = .1f;

    // otimização nos calculos de colisão
    if(glm::distance(point, position) > (scl+offset)) {
        return false;
    }
    
    // minimos
    glm::vec3 minx = position + (glm::vec3(-1, 0, 0) * (scl+offset));
    glm::vec3 miny = position + (glm::vec3(0, -1, 0) * (scl+offset));
    glm::vec3 minz = position + (glm::vec3(0, 0, -1) * (scl+offset));
    // maximos
    glm::vec3 maxx = position + (glm::vec3(1, 0, 0) * (scl+offset));
    glm::vec3 maxy = position + (glm::vec3(0, 1, 0) * (scl+offset));
    glm::vec3 maxz = position + (glm::vec3(0, 0, 1) * (scl+offset));

    return (
        (point.x >= minx.x && point.x <= maxx.x) &&
        (point.y >= miny.y && point.y <= maxy.y) &&
        (point.z >= minz.z && point.z <= maxz.z)
    );
}

void Cube::process() {
    {
        //---- TRIANGLE --------///
        // vertices = {
        //     {0.5f,  0.5f, 0.0f},  // top right
        //     {0.5f, -0.5f, 0.0f},  // bottom right
        //     {-0.5f, -0.5f, 0.0f},  // bottom left
        //     {-0.5f,  0.5f, 0.0f}   // top left 
        // };

        // indices = {  // note that we start from 0!
        //     0, 1, 3,  // first Triangle
        //     1, 2, 3   // second Triangle
        // };
    }

    std::vector<Vertex> vertices = {
    //      POSITION            NORMAL     UV      TANGENT  BITANGENT
        {{-0.5f, 0.0f, 0.5f },{0, 0, 0},{0.0,0.0},{0, 0, 0},{0, 0, 0}},  
        {{0.5f, 0.0f, 0.5f  },{0, 0, 0},{1.0,0.0},{0, 0, 0},{0, 0, 0}},
        {{0.5f, 1.0f, 0.5f  },{0, 0, 0},{1.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 1.0f, 0.5f },{0, 0, 0},{0.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 1.0f, -0.5f},{0, 0, 0},{0.0,0.0},{0, 0, 0},{0, 0, 0}},
        {{0.5f, 1.0f, -0.5f },{0, 0, 0},{1.0,0.0},{0, 0, 0},{0, 0, 0}},
        {{0.5f, 0.0f, -0.5f },{0, 0, 0},{1.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 0.0f, -0.5f},{0, 0, 0},{0.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{0.5f, 0.0f, 0.5f  },{0, 0, 0},{0.0,0.0},{0, 0, 0},{0, 0, 0}}, 
        {{0.5f, 0.0f, -0.5f },{0, 0, 0},{1.0,0.0},{0, 0, 0},{0, 0, 0}},
        {{0.5f, 1.0f, -0.5f },{0, 0, 0},{1.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{0.5f, 1.0f, 0.5f  },{0, 0, 0},{0.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 0.0f, -0.5f},{0, 0, 0},{0.0,0.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 0.0f, 0.5f },{0, 0, 0},{1.0,0.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 1.0f, 0.5f },{0, 0, 0},{1.0,1.0},{0, 0, 0},{0, 0, 0}},
        {{-0.5f, 1.0f, -0.5f},{0, 0, 0},{0.0,1.0},{0, 0, 0},{0, 0, 0}}
    };

    indices = {
        0,1,2,
        3,4,5,
        6,7,3,
        2,5,4,
        7,6,1,
        0,8,9,
        10,11,12,
        13,14,15
    };

    // vertex array
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);

    // passando o array de vertices
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    // passando o array de indices
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0],  GL_STATIC_DRAW);

    // atributo de vertices
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);

    // atributo de normais
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, normal));

    // atributo de textura
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, texture));

    glBindVertexArray(0);

}

#endif