#ifndef ENGINE_TIMER_H
#define ENGINE_TIMER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <functional>

typedef std::function<void()> EngineTimerCallback;

class EngineTimer
{
private:
    bool lock;
    int time; // current time
    int tick; // in miliseconds
    int accumulator; // current time past
    EngineTimerCallback engineTimerCallback;

    int getTimeInMilliseconds();
public:
    EngineTimer();
    EngineTimer(int tick, EngineTimerCallback engineTimerCallback);
    ~EngineTimer();

    void process();
    void setTick(int tick);
    void setEngineTimerCallback(EngineTimerCallback engineTimerCallback);

};

#endif