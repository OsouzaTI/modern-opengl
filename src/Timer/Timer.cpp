#include <Timer/Timer.h>

EngineTimer::EngineTimer() : lock(false) {}

EngineTimer::EngineTimer(int tick, EngineTimerCallback engineTimerCallback)
    : tick(tick), engineTimerCallback(engineTimerCallback), lock(false)
{

}

EngineTimer::~EngineTimer()
{
}

void EngineTimer::setTick(int tick) {
    this->tick = tick;
}

void EngineTimer::setEngineTimerCallback(EngineTimerCallback engineTimerCallback) {
    this->engineTimerCallback = engineTimerCallback;
}

int EngineTimer::getTimeInMilliseconds() {
    return glfwGetTime() * 1000;
}

void EngineTimer::process() {

    if(!lock) {
        time = getTimeInMilliseconds();
        lock = true; 
    }

    if(getTimeInMilliseconds() - time >= tick) {
        engineTimerCallback();
        lock = false; 
    }

}