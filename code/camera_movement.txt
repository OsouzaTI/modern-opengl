
    if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
        Engine::camera.processKeyboard(LEFT, deltaTime);
    } 
    
    if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
        Engine::camera.processKeyboard(RIGHT, deltaTime);
    }

    if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
        Engine::camera.processKeyboard(FORWARD, deltaTime);
    }

    if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
        Engine::camera.processKeyboard(BACKWARD, deltaTime);
    }