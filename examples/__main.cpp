#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "src/cube.hpp"
#include "src/camera.hpp"
#include "src/texture2d.h"
#include "src/resource_manager.h"

void processKeyboard(GLFWwindow* window);

// mapa
std::string mapa = "\
*********************************************************************   *\
*   *               *               *           *                   *   *\
*   *   *********   *   *****   *********   *****   *****   *****   *   *\
*               *       *   *           *           *   *   *       *   *\
*********   *   *********   *********   *****   *   *   *   *********   *\
*       *   *               *           *   *   *   *   *           *   *\
*   *   *************   *   *   *********   *****   *   *********   *   *\
*   *               *   *   *       *           *           *       *   *\
*   *************   *****   *****   *   *****   *********   *   *****   *\
*           *       *   *       *   *       *           *   *           *\
*   *****   *****   *   *****   *   *********   *   *   *   *************\
*       *       *   *   *       *       *       *   *   *       *       *\
*************   *   *   *   *********   *   *****   *   *****   *****   *\
*           *   *           *       *   *       *   *       *           *\
*   *****   *   *********   *****   *   *****   *****   *************   *\
*   *       *           *           *       *   *   *               *   *\
*   *   *********   *   *****   *********   *   *   *************   *   *\
*   *           *   *   *   *   *           *               *   *       *\
*   *********   *   *   *   *****   *********   *********   *   *********\
*   *       *   *   *           *           *   *       *               *\
*   *   *****   *****   *****   *********   *****   *   *********   *   *\
*   *                   *           *               *               *   *\
*   *********************************************************************\
";

// settings
unsigned int WIDTH = 1280;
unsigned int HEIGHT = 720;

Camera camera(glm::vec3(-100, -10, ));

float deltaTime = 0.0f;
float lastFrame = 0.0f;

bool keys[1024];
void keyboardCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
   
    if(action == GLFW_PRESS) {
        keys[key] = true;
    }

    if(action == GLFW_RELEASE) {
        keys[key] = false;
    }

}

void mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    camera.updateMouse(xpos, ypos);
}

void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
    WIDTH = width;
    HEIGHT = height;
}

int main(void)
{
    GLFWwindow* window;

    /* Initialize the library */
    if (!glfwInit())
        return EXIT_FAILURE;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World", NULL, NULL);

    if (!window)
    {
        glfwTerminate();
        return EXIT_FAILURE;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    glfwSetKeyCallback(window, keyboardCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    
    // desabilitando o mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    
    // flip nas imagens carregadas pelo stb
    stbi_set_flip_vertically_on_load(true);

    if(glewInit() != GLEW_OK) {
        return EXIT_FAILURE;
    }
    
    glEnable(GL_DEPTH_TEST);

    Cube cubo;
    Shader shader = ResourceManager::loadShader("shaders/model.vert", "shaders/model.frag", NULL, "cube");
    Texture2D texture = ResourceManager::loadTexture("assets/textures/brickwall.jpg", false, "brick");
    Texture2D ground = ResourceManager::loadTexture("assets/textures/ground.jpg", false, "ground");
    Texture2D sky = ResourceManager::loadTexture("assets/textures/bluesky.jpg", false, "sky");
    Texture2D brick = ResourceManager::loadTexture("assets/textures/brick02.png", false, "brick02");
    Texture2D ceil = ResourceManager::loadTexture("assets/textures/brick03.png", true, "brick03");

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glEnable(GL_CULL_FACE);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window))
    {
        // atualizando delta time
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        camera.lockAxis(0, 1, 0);

        processKeyboard(window);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // ativando o shader
        shader.use();

        // matriz de projeção
        glm::mat4 projection = glm::perspective(glm::radians(45.0f), WIDTH/(float)HEIGHT, 0.1f, 250.0f);
        shader.setMatrix4("projection", projection);        
    
        glm::mat4 view = camera.getViewMatrix();
        shader.setMatrix4("view", view);  

        glCullFace(GL_BACK);
        cubo.multipleRender(mapa.length(), [&shader, &texture, &ground, &brick](int i, Object& cubo){

            float scale = 1.0f;
            
            int w = 73;
            int x = i%w;
            int y = i/w;
            int index = y * w + x;
   
            glm::vec3 pos = glm::vec3(x*scale, scale, (-y - 10)*scale);

            if(mapa[index] == '*') { // parede
                texture.bind(); 
                cubo.translate(pos);                
            } else if(mapa[index] == ' ') { // chão
                ground.bind();
                cubo.translate(pos - glm::vec3(0, scale, 0));
            }

            // testando colisão do cubo
            if(cubo.isPointInside(camera.getCameraPosition())) {
                brick.bind();
                camera.setPosition(camera.getLastPosition());
            }
            
            cubo.scale(scale);
            shader.setMatrix4("model", cubo.getModelMatrix());
            cubo.render();
        });
        
        glfwSwapBuffers(window);        
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

void processKeyboard(GLFWwindow* window) {
    // fecha a janela
    if(keys[GLFW_KEY_ESCAPE]) {
        glfwSetWindowShouldClose(window, true);
    }

    if(keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT]) {
        camera.processKeyboard(LEFT, deltaTime);
    }
    
    if(keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT]) {
        camera.processKeyboard(RIGHT, deltaTime);
    }

    if(keys[GLFW_KEY_W] || keys[GLFW_KEY_UP]) {
        camera.processKeyboard(FORWARD, deltaTime);
    }

    if(keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN]) {
        camera.processKeyboard(BACKWARD, deltaTime);
    }
    
}