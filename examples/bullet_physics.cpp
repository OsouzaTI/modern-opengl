#include <iostream>
#include <core.h>

#include <Serialize/BulletFileLoader/btBulletFile.h>
#include <Serialize/BulletWorldImporter/btBulletWorldImporter.h>

#define N_COLORS 10

enum 
{
    GROUND_OBJECT,
    CUBE_OBJECT,
    SPHERE_OBJECT
}; 

glm::vec3 COLORS[N_COLORS];

Engine engine(1280, 720);
EntityObject ground;
EntityObject esfera;
EntityObject terrain;

// ------------------- SHADERS -----------------//
Shader EsferaShader;
Shader EsferaLightShader;
Shader EsferaTwoLightShader;

void carregandoShaders()
{
    EsferaShader = LOAD_SHADER("shaders/model.vert", "shaders/model.frag", NULL, "EsferaShader");
    EsferaLightShader = LOAD_SHADER("shaders/directional_light.vert", "shaders/directional_light.frag", NULL, "EsferaLightShader");
    EsferaTwoLightShader = LOAD_SHADER("shaders/two_directional_light.vert", "shaders/two_directional_light.frag", NULL, "EsferaTwoLightShader");
}

// ------------------ TEXTURES ---------------//

Texture2D EsferaTexture;
Texture2D CuboRedTexture;
Texture2D GroundTexture;

void carregandoTexturas()
{
    EsferaTexture = LOAD_TEXTURE("assets/textures/brick02.png", true, "EsferaTexture");
    GroundTexture = LOAD_TEXTURE("assets/textures/dark_green_solid.png", true, "GroundTexture");
    CuboRedTexture = LOAD_TEXTURE("assets/textures/dark_red_solid.png", true, "CuboRedTexture");
}


void carregandoModelos()
{
    ground.loadModel("assets/objects/cube/cube.obj");
    esfera.loadModel("assets/objects/sphere/sphere02.obj");
    terrain.loadModel("assets/objects/terrain02/terrain02.obj");
}

void gerandoCores()
{
    for (int cor = 0; cor < N_COLORS; cor++)
    {
        COLORS[cor] = glm::vec3{
            (rand()%100)/100.0f,
            (rand()%100)/100.0f,
            (rand()%100)/100.0f
        };
    }
    
}

// --- Colision Shapes
btWorldImporter* importer;
btAlignedObjectArray<btCollisionShape*> collisionShapes;
// chão
int selectedGroundId = 0;
btAlignedObjectArray<btRigidBody*> grounds;

void rotacionaChao(glm::vec3 axis, float angle, unsigned int groundId)
{
    btTransform trans;
    grounds[groundId]->getMotionState()->getWorldTransform(trans);
    trans.setRotation(btQuaternion(glm3bt(axis), angle));
    grounds[groundId]->getMotionState()->setWorldTransform(trans);
    grounds[groundId]->setWorldTransform(trans);
}

int corAleatoria()
{
    return rand() % N_COLORS;
}

void criandoChao(btVector3 size, btVector3 position)
{
    btTriangleMesh* tm = std::static_pointer_cast<AssimpModel>(terrain.getModel3D())->btMesh;
    btCollisionShape* groundShape = new btBvhTriangleMeshShape(tm, true);
    //new btBoxShape(size);
    
    collisionShapes.push_back(groundShape);
    btTransform groundTransform;
    groundTransform.setIdentity();
    groundTransform.setOrigin(position);
    btScalar mass(0.);

    //rigidbody is dynamic if and only if mass is non zero, otherwise static
    bool isDynamic = (mass != 0.f);

    btVector3 localInertia(0, 0, 0);
    if (isDynamic)
        groundShape->calculateLocalInertia(mass, localInertia);

    //using motionstate is optional, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(groundTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
    btRigidBody* rbGround = new btRigidBody(rbInfo);
    rbGround->setUserIndex(GROUND_OBJECT);
    grounds.push_back(rbGround);
    //add the body to the dynamics world
    engine.getBtDynamicsWorld()->addRigidBody(rbGround);
}

void criandoEsfera()
{
    btCollisionShape* colShape = importer->createSphereShape(btScalar(1.));
    
    // calculando posição do objeto
    btTransform startTransform;
    startTransform.setIdentity();

    // inicializando atributos físicos
    btScalar mass(1.f);
    btVector3 localInertia(0, 0, 0);
    // como é um objeto dinamico, necessita calcular a inercia
    colShape->calculateLocalInertia(mass, localInertia);
    
    // posicionando o objeto no mundo
    startTransform.setOrigin(btVector3(2, 10, 0));

    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* rbEsfera = new btRigidBody(rbInfo);
    rbEsfera->setUserIndex(SPHERE_OBJECT);
    rbEsfera->setUserIndex2(corAleatoria());
    engine.getBtDynamicsWorld()->addRigidBody(rbEsfera);
}

void criandoCubo()
{
    btCollisionShape* colShape = importer->createBoxShape({1, 1, 1});
    // posicionando-o no mundo
    btTransform startTransform;
    startTransform.setIdentity();
    startTransform.setOrigin(btVector3(2, 10, 0));
    // inicializando atributos físicos
    btScalar mass(1.f);
    btVector3 localInertia(0, 0, 0);
    // como é um objeto dinamico, necessita calcular a inercia
    colShape->calculateLocalInertia(mass, localInertia);
    //using motionstate is recommended, it provides interpolation capabilities, and only synchronizes 'active' objects
    btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
    btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
    btRigidBody* rigidBody = new btRigidBody(rbInfo);
    rigidBody->setUserIndex(CUBE_OBJECT);
    rigidBody->setUserIndex2(corAleatoria());
    engine.getBtDynamicsWorld()->addRigidBody(rigidBody);
}

int main(void)
{
    
    importer = new btWorldImporter(engine.getBtDynamicsWorld());

    
    carregandoShaders();
    carregandoTexturas();
    carregandoModelos();
    gerandoCores();

    criandoChao({10, 1, 10}, {0, -2, 0});

    float groundAngle = 0.0f;
    glm::vec3 groundRotation{1, 0, 1};

    DirectionalLight light{1.0f, {0, -1, 0}, {1, 1, 1}};

    bool ORBITAL = false;
    Engine::getCamera().setFront(glm::vec3(0, -0.999848, -glm::radians(120.0f)));
    Engine::setCameraPosition(glm::vec3(0, 15, 30));

    engine.setDebugMode(true);
    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
        
        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

        if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
            Engine::getCamera().processKeyboard(LEFT, deltaTime);
        } 
        
        if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
            Engine::getCamera().processKeyboard(RIGHT, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
            Engine::getCamera().processKeyboard(FORWARD, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
            Engine::getCamera().processKeyboard(BACKWARD, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_ENTER)) {
            ORBITAL = !ORBITAL;
            Engine::getCamera().setOrbitalCamera(ORBITAL);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {
        // groundAngle = cosf(glm::radians(glfwGetTime()*50.0f))/4.0f;
        rotacionaChao(groundRotation, groundAngle, selectedGroundId);        
    });

    engine.setRenderCallback([&](float dt) -> void {
        
        { // ImGui
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

                ImGui::Begin("Criar Objetos");
                    if(ImGui::Button("Esfera"))
                        criandoEsfera();
                    if(ImGui::Button("Cubo"))
                        criandoCubo();
                ImGui::End();

                ImGui::Begin("Iluminação");
                    ImGui::SliderFloat3("Position", &light.direction[0], -50.0, 50.0f);
                    ImGui::SliderFloat("Intensity", &light.intensity, 0.0, 1.0f);
                    ImGui::SliderFloat3("Light Color", &light.color[0], 0.0f, 1.0f);
                ImGui::End();

                ImGui::Begin("Ground Shape");
                    ImGui::SliderInt("Ground Id", &selectedGroundId, 0, grounds.size()-1);
                    ImGui::SliderFloat("Angle", &groundAngle, -M_PI, M_PI);
                    ImGui::SliderFloat3("Rotation", &groundRotation[0], 0.0f, 1.0f);
                ImGui::End();

                ImGui::Begin("Camera");
                    if(ImGui::Checkbox("Orbital camera", &ORBITAL))
                        Engine::getCamera().setOrbitalCamera(ORBITAL);

                ImGui::End();

            ImGui::EndFrame();
        }

        btDynamicsWorld* world = engine.getBtDynamicsWorld();
        for (int j = 0; j < world->getNumCollisionObjects(); j++)
		{
			btCollisionObject* obj = world->getCollisionObjectArray()[j];
			btRigidBody* body = btRigidBody::upcast(obj);
			btTransform trans;


            EsferaLightShader.bind();
            EsferaLightShader.setFloat("intensity", light.intensity);
            EsferaLightShader.setVector3f("light", light.direction);

            int cor = body->getUserIndex2();
            EsferaLightShader.setVector3f("lightColor", COLORS[cor]);
    
			if (body && body->getMotionState())
			{
                body->getMotionState()->getWorldTransform(trans);     
                if(body->getUserIndex() == SPHERE_OBJECT)
                {
                    EsferaTexture.bind();                    
                    esfera.getTransform()->setIdentityMatrix();
                    esfera.getTransform()->setPosition(bt3glm(trans.getOrigin()));                    
                    esfera.getTransform()->setRotate(trans.getRotation());
                    esfera.render(EsferaLightShader);
                } 
                else if(body->getUserIndex() == CUBE_OBJECT) 
                {
                    CuboRedTexture.bind();
                    btBoxShape* bs = static_cast<btBoxShape*>(obj->getCollisionShape());                    
                    ground.getTransform()->setIdentityMatrix();
                    ground.getTransform()->setPosition(bt3glm(trans.getOrigin()));                    
                    ground.getTransform()->setRotate(trans.getRotation());
                    ground.getTransform()->setScale(bt3glm(bs->getHalfExtentsWithoutMargin()));     
                    ground.render(EsferaLightShader);
                }
                else if(body->getUserIndex() == GROUND_OBJECT)
                {
                    // GroundTexture.bind();
                    EsferaLightShader.setVector3f("lightColor", light.color);
                    terrain.getTransform()->setIdentityMatrix();
                    terrain.getTransform()->setPosition(bt3glm(trans.getOrigin()));                    
                    terrain.getTransform()->setRotate(trans.getRotation());
                    terrain.render(EsferaLightShader);
                }

			}
			else
			{
                trans = obj->getWorldTransform();		
        	}
       
		}



    });

    engine.run();

    return 0;
}