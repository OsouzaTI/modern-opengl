#include <iostream>
#include <queue>
#include <core.h>

#define SNAKE_LEFT  glm::vec2(-1,  0)
#define SNAKE_RIGHT glm::vec2( 1,  0)
#define SNAKE_UP    glm::vec2( 0,  1)
#define SNAKE_DOWN  glm::vec2( 0, -1)
#define SNAKE_INITIAL_POSITION glm::vec3(0, -8, 0)
#define GRID_SIZE 15
#define CELL_SIZE 2

struct
{
    int size;
    EngineTimer tick;
    glm::vec2 direction;
    EntityObject enity;
    EntityObject tailEntity;
    std::queue<glm::vec3> tail;
    glm::vec3 lastPosition;
    glm::vec3 getDirection3D()
    {
        return glm::vec3(direction.x, 0, -direction.y);
    }

    void eat()
    {
        tail.push(lastPosition);
        size++;
    }

} snake;

Engine engine(1280, 720);
EntityObject ground;
EntityObject food;

float LightAngle = 0.0f;

EntityObject SunLight;
EntityObject MoonLight;

DirectionalLight Sun{1.0f, glm::vec3(0, 10.0f, 0), glm::vec3(0.5, 0.5, 0.0)};
DirectionalLight Moon{1.0f, glm::vec3(0, -10.0f, 0), glm::vec3(1.0, 1.0, 1.0)};
DirectionalLight Snake{0.2f, glm::vec3(0, -10.0f, 0), glm::vec3(1.0, 1.0, 1.0)};
DirectionalLight Food{0.2f, glm::vec3(0, -10.0f, 0), glm::vec3(1.0, 0.0, 0.0)};

// ------------------- SHADERS -----------------//
Shader SnakeShader;
Shader SnakeLightShader;
Shader SnakeTwoLightShader;

void carregandoShaders()
{
    SnakeShader = LOAD_SHADER("shaders/model.vert", "shaders/model.frag", NULL, "SnakeShader");
    SnakeLightShader = LOAD_SHADER("shaders/directional_light.vert", "shaders/directional_light.frag", NULL, "SnakeShader");
    SnakeTwoLightShader = LOAD_SHADER("shaders/two_directional_light.vert", "shaders/two_directional_light.frag", NULL, "SnakeShader");
}

// ------------------ TEXTURES ---------------//

Texture2D SnakeTexture;
Texture2D GroundTexture;
Texture2D SnakeBody;
Texture2D SunTexture;
Texture2D MoonTexture;
Texture2D FoodTexture;

void carregandoTexturas()
{
    SnakeTexture = LOAD_TEXTURE("assets/textures/snake_head.png", true, "SnakeTexture");
    GroundTexture = LOAD_TEXTURE("assets/textures/brick03.png", true, "GroundTexture");
    SnakeBody = LOAD_TEXTURE("assets/textures/Snake.png", true, "SnakeBody");
    SunTexture = LOAD_TEXTURE("assets/textures/sun02.jpg", false, "SunTexture");
    MoonTexture = LOAD_TEXTURE("assets/textures/moon.jpg", false, "MoonTexture");
    FoodTexture = LOAD_TEXTURE("assets/textures/apple.jpg", false, "FoodTexture");
}

void controlarLuz()
{
    float radius = 30.0f;
    float speed = 5.0f;
    
    // Sun.direction = glm::vec3(
    //     13.5f, 
    //     -8.0f + radius * cosf(glm::radians(glfwGetTime() * speed)), 
    //     -8.0f + radius * sinf(glm::radians(glfwGetTime() * speed))
    // );

    // Moon.direction = glm::vec3(
    //     13.5f, 
    //     -8.0f + -radius * cosf(glm::radians(glfwGetTime() * speed)), 
    //     -8.0f + -radius * sinf(glm::radians(glfwGetTime() * speed))
    // );

    Snake.direction = snake.enity.getTransform()->getPosition();
    Food.direction = food.getTransform()->getPosition();

    SunLight.getTransform()->setPosition(Sun.direction);
    MoonLight.getTransform()->setPosition(Moon.direction);
}

void gerarComida() 
{
    // gerando uma posição valida do gride
    int foodX = (rand()%GRID_SIZE) * CELL_SIZE;
    int foodY = (rand()%GRID_SIZE) * CELL_SIZE;
    glm::vec3 foodPosition = glm::vec3(foodX, -8, -foodY);
    food.getTransform()->setPosition(foodPosition);
}

void configurandoComida()
{
    food.loadModel("assets/objects/sphere/sphere.obj");
    food.getTransform()->setPositionY(-6);
    food.getTransform()->setScale(glm::vec3(.4f, .4f, .4f));
    food.setRenderCollider(true);
    gerarComida();
}

void configurandoCobra() 
{
    snake.size = 0;
    snake.direction = glm::vec2(0, 0);
    snake.enity.loadModel("assets/objects/snake_body/esfera.obj");
    snake.tailEntity.loadModel("assets/objects/snake_body/esfera.obj");
    snake.enity.getTransform()->setPosition(SNAKE_INITIAL_POSITION);
    snake.tick.setTick(250);

    snake.tick.setEngineTimerCallback([&](){
        
        snake.lastPosition = snake.enity.getTransform()->getPosition();
        snake.enity.getTransform()->setPosition(snake.lastPosition + (float)CELL_SIZE * snake.getDirection3D());

        if(snake.size > 0) {
            std::queue<glm::vec3> oldTail;
            for (int i = 0; i < snake.size - 1; i++)
            {
                // guardei a informação de todos os elementos menos o ultimo
                oldTail.push(snake.tail.front());
                snake.tail.pop();
            }
            // removendo o utlimo elemento da cauda
            snake.tail.pop();
            // adicionando ultima posição anterior a cabeça
            snake.tail.push(snake.lastPosition);
            // reprocessando cauda
            while (!oldTail.empty())
            {
                snake.tail.push(oldTail.front());
                oldTail.pop();
            }            
        }
        
    
    });

}

void configurandoChao()
{
    ground.loadModel("assets/objects/cube/cube.obj");
}

int main(void)
{
    carregandoShaders();
    carregandoTexturas();
    configurandoCobra();
    configurandoChao();
    configurandoComida();

    engine.setDebugMode(true);
    
    bool SNAKE_MOVEMENT = false;
    bool ORBITAL_CAMERA = false;
    float FOV = 60.0f;
    float SPEED = 10.0f;
    glm::vec3 CAMERA_POSITION = glm::vec3(13.5f, 8, 8);

    Engine::getCamera().setPosition(CAMERA_POSITION);
    Engine::getCamera().setFront(glm::vec3(0, -0.999848, -glm::radians(60.0f)));

    // ---------------- LIGHT -----------//
    SunLight.loadModel("assets/objects/sphere/sphere.obj");
    SunLight.getTransform()->setPosition(Sun.direction);
    SunLight.getTransform()->setScale(glm::vec3(.5f, .5f, .5f));

    MoonLight.loadModel("assets/objects/sphere/sphere.obj");
    MoonLight.getTransform()->setPosition(Moon.direction);
    MoonLight.getTransform()->setScale(glm::vec3(.5f, .5f, .5f));

    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
        
        Engine::getCamera().setSpeed(SPEED);
        Engine::getCamera().setFov(FOV);
        // Engine::getCamera().setPosition(CAMERA_POSITION);
        Engine::getCamera().setOrbitalCamera(ORBITAL_CAMERA);
        
        snake.tick.process();
        controlarLuz();

        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }
        
        if(SNAKE_MOVEMENT) {

            if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
                if(snake.direction * -1.0f != SNAKE_LEFT)
                    snake.direction = SNAKE_LEFT;
            } 
            
            if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
                if(snake.direction * -1.0f != SNAKE_RIGHT)
                    snake.direction = SNAKE_RIGHT;
            }

            if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
                if(snake.direction * -1.0f != SNAKE_UP)
                    snake.direction = SNAKE_UP;
            }

            if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
                if(snake.direction * -1.0f != SNAKE_DOWN)
                    snake.direction = SNAKE_DOWN;
            }

        }

        if(ORBITAL_CAMERA)
        {
            if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
                Engine::getCamera().processKeyboard(LEFT, deltaTime);
            } 
            
            if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
                Engine::getCamera().processKeyboard(RIGHT, deltaTime);
            }

            if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
                Engine::getCamera().processKeyboard(FORWARD, deltaTime);
            }

            if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
                Engine::getCamera().processKeyboard(BACKWARD, deltaTime);
            }
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {

        if(snake.enity.getCollider()->isColliding(food.getTransform()->getPosition())) {
            std::cout << "Colidiu" << std::endl;
            snake.eat();
            gerarComida();
        }

    });

    engine.setRenderCallback([&](float dt) -> void {
        
        { // ImGui
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
                
                ImGui::Begin("Game");
                    ImGui::Checkbox("Snake Move", &SNAKE_MOVEMENT);
                    ImGui::Separator();
                    ImGui::Text("Camera Settings");
                    ImGui::Checkbox("Orbital", &ORBITAL_CAMERA);
                    ImGui::SliderFloat("FOV", &FOV, 45.0f, 120.0f);
                    ImGui::SliderFloat("Speed", &SPEED, 0.0f, 100.0f);
                    ImGui::SliderFloat3("Position", &CAMERA_POSITION[0], -100.0f, 100.0f);
                    ImGui::Separator();
                    ImGui::Text("Light Positions");
                    ImGui::SliderFloat3("Light A", &Sun.direction[0], -100.0f, 100.0f);
                
                ImGui::End();

            ImGui::EndFrame();
        }

        SnakeShader.bind();
        SunTexture.bind();
        SunLight.getTransform()->pushMatrix();
        SunLight.render(SnakeShader);

        MoonTexture.bind();
        MoonLight.getTransform()->pushMatrix();
        MoonLight.render(SnakeShader);

        SnakeTwoLightShader.bind();
        SnakeTwoLightShader.setFloat("intensityA", Sun.intensity);
        SnakeTwoLightShader.setVector3f("lightA", Sun.direction);
        SnakeTwoLightShader.setVector3f("lightColorA", Sun.color);

        SnakeTwoLightShader.setFloat("intensityB", Moon.intensity);
        SnakeTwoLightShader.setVector3f("lightB", Moon.direction);
        SnakeTwoLightShader.setVector3f("lightColorB", Moon.color);

        SnakeTwoLightShader.setFloat("intensityC", Snake.intensity);
        SnakeTwoLightShader.setVector3f("lightC", Snake.direction);
        SnakeTwoLightShader.setVector3f("lightColorC", Snake.color);

        SnakeTwoLightShader.setFloat("intensityD", Food.intensity);
        SnakeTwoLightShader.setVector3f("lightD", Food.direction);
        SnakeTwoLightShader.setVector3f("lightColorD", Food.color);

        for (int W = 0; W < GRID_SIZE; W++)
        {
            for (int H = 0; H < GRID_SIZE; H++)
            {
                GroundTexture.bind();
                ground.getTransform()->setIdentityMatrix();
                ground.getTransform()->setPosition(glm::vec3(CELL_SIZE * W, -10, -CELL_SIZE * H));
                ground.render(SnakeTwoLightShader);
            }
            
        }
        
        FoodTexture.bind();
        food.getTransform()->pushMatrix();
        food.render(SnakeTwoLightShader);

        SnakeTexture.bind();
        snake.enity.getTransform()->pushMatrix();
        snake.enity.render(SnakeTwoLightShader);

        // desenhando cauda
        std::queue<glm::vec3> tail = snake.tail;
        while(!tail.empty())
        {
            SnakeBody.bind();    
            snake.tailEntity.getTransform()->setIdentityMatrix();
            snake.tailEntity.getTransform()->setPosition(tail.front());
            snake.tailEntity.render(SnakeShader);
            tail.pop();
        }

    });

    engine.run();

    return 0;
}