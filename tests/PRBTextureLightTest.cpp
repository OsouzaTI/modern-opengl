#include <iostream>
#include <queue>
#include <EngineCore.h>
#include <Core/PBRTexture.h>
#include <Model/BasicMesh.h>

unsigned int loadTexture(char const * path);

Engine engine(1280, 720);
DirectionalLight light{1.0, {0, 0, 0}, glm::vec3(150.0f, 150.0f, 150.0f)};
BasicMesh esfera, cubo;

unsigned int sphereVAO = 0;
unsigned int indexCount;
void renderSphere()
{
    if (sphereVAO == 0)
    {
        glGenVertexArrays(1, &sphereVAO);

        unsigned int vbo, ebo;
        glGenBuffers(1, &vbo);
        glGenBuffers(1, &ebo);

        std::vector<glm::vec3> positions;
        std::vector<glm::vec2> uv;
        std::vector<glm::vec3> normals;
        std::vector<unsigned int> indices;

        const unsigned int X_SEGMENTS = 64;
        const unsigned int Y_SEGMENTS = 64;
        const float PI = 3.14159265359f;
        for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
        {
            for (unsigned int y = 0; y <= Y_SEGMENTS; ++y)
            {
                float xSegment = (float)x / (float)X_SEGMENTS;
                float ySegment = (float)y / (float)Y_SEGMENTS;
                float xPos = std::cos(xSegment * 2.0f * PI) * std::sin(ySegment * PI);
                float yPos = std::cos(ySegment * PI);
                float zPos = std::sin(xSegment * 2.0f * PI) * std::sin(ySegment * PI);

                positions.push_back(glm::vec3(xPos, yPos, zPos));
                uv.push_back(glm::vec2(xSegment, ySegment));
                normals.push_back(glm::vec3(xPos, yPos, zPos));
            }
        }

        bool oddRow = false;
        for (unsigned int y = 0; y < Y_SEGMENTS; ++y)
        {
            if (!oddRow) // even rows: y == 0, y == 2; and so on
            {
                for (unsigned int x = 0; x <= X_SEGMENTS; ++x)
                {
                    indices.push_back(y * (X_SEGMENTS + 1) + x);
                    indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                }
            }
            else
            {
                for (int x = X_SEGMENTS; x >= 0; --x)
                {
                    indices.push_back((y + 1) * (X_SEGMENTS + 1) + x);
                    indices.push_back(y * (X_SEGMENTS + 1) + x);
                }
            }
            oddRow = !oddRow;
        }
        indexCount = static_cast<unsigned int>(indices.size());

        std::vector<float> data;
        for (unsigned int i = 0; i < positions.size(); ++i)
        {
            data.push_back(positions[i].x);
            data.push_back(positions[i].y);
            data.push_back(positions[i].z);
            if (normals.size() > 0)
            {
                data.push_back(normals[i].x);
                data.push_back(normals[i].y);
                data.push_back(normals[i].z);
            }
            if (uv.size() > 0)
            {
                data.push_back(uv[i].x);
                data.push_back(uv[i].y);
            }
        }
        glBindVertexArray(sphereVAO);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(float), &data[0], GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);
        unsigned int stride = (3 + 2 + 3) * sizeof(float);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, stride, (void*)0);
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, stride, (void*)(3 * sizeof(float)));
        glEnableVertexAttribArray(2);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, (void*)(6 * sizeof(float)));
    }

    glBindVertexArray(sphereVAO);
    glDrawElements(GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}


int main(void)
{
    
    engine.setDebugMode(true);
    // Engine::getCamera().setOrbitalCamera(true);
    // engine.setHideMouse(true);
    Engine::getCamera().setSpeed(20.0f);

    esfera.loadMesh("assets/objects/sphere/sphere02.obj");
    cubo.loadMesh("assets/objects/cube/cube.obj");
    

    engine.loadShader("light/pbr_light_textured", "PBR");
    engine.loadShader("model_solid", "Solid");
    
    // load PBR material textures
    // --------------------------
    PBRTexture texture(
        "PBR/rustediron/rustediron2_basecolor.png",
        "PBR/rustediron/rustediron2_normal.png",
        "PBR/rustediron/rustediron2_metallic.png",
        "PBR/rustediron/rustediron2_roughness.png",
        "PBR/reinforced-metal/reinforced-metal_ao.png"
    );

    Engine::setCameraPosition({0, 0, 0});

    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
            
        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }


        if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
            Engine::getCamera().processKeyboard(LEFT, deltaTime);
        } 
        
        if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
            Engine::getCamera().processKeyboard(RIGHT, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
            Engine::getCamera().processKeyboard(FORWARD, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
            Engine::getCamera().processKeyboard(BACKWARD, deltaTime);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {

    });

    Shader& shader = engine.getShader("PBR");
    Shader& solid  = engine.getShader("Solid"); 

    engine.setRenderCallback([&](float dt) -> void {
        
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

                ImGui::Begin("Light");
                    ImGui::Text("Settings");
                    ImGui::SliderFloat3("Color", &light.color[0], 0.0f, 255.0f);
                    ImGui::InputFloat3("Position", &light.direction[0]);
                ImGui::End();
            
            ImGui::EndFrame();
        }

        float angle = glm::radians(glfwGetTime() * 100.0f);
        light.direction = glm::vec3{0, 0, 0} 
            + 4.0f * glm::vec3{cosf(angle), sinf(angle), 0};

        glm::mat4 lightModel{1.0f};
        lightModel = glm::translate(lightModel, light.direction);        
        lightModel = glm::scale(lightModel, glm::vec3{0.3f}); 

        solid.bind();
        solid.setVector3f("color", glm::normalize(light.color));
        solid.setMatrix4("projection", Engine::getProjectionMatrix());
        solid.setMatrix4("view", Engine::getViewMatrix());
        solid.setMatrix4("model", lightModel);
        cubo.render();

    
        // seta todas as texturas
        texture.bind();
        shader.bind();
        shader.setMatrix4("projection", Engine::getProjectionMatrix());
        shader.setInteger("albedoMap", 0);
        shader.setInteger("normalMap", 1);
        shader.setInteger("metallicMap", 2);
        shader.setInteger("roughnessMap", 3);
        shader.setInteger("aoMap", 4);
        shader.setDirectionalLight(light);
        shader.setVector3f("camPos", Engine::getCameraPosition());
        shader.setMatrix4("view", Engine::getViewMatrix());

        glm::mat4 model{1.0f};
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                model = glm::mat4(1.0f);
                model = glm::translate(model, {i * 2.5, j * 2.5, -10});              
                shader.setMatrix4("model", model);
                renderSphere();   
            }         
        }
        

    });

    engine.run();

    return 0;
}


unsigned int loadTexture(char const * path)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}