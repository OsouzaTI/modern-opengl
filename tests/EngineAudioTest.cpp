#include <iostream>
#include <queue>
#include <EngineCore.h>

Engine engine(1280, 720);

EntityObject sphere;

int main(void)
{
    // Preparando assets
    engine.loadShader("model", "default");
    engine.loadTexture("dark_green_solid.png", "default", true);

    Shader& Default = engine.getShader("default");
    Texture2D& Texture = engine.getTexture("default");

    sphere.loadModel("assets/objects/sphere/sphere02.obj");
    sphere.setRenderCollider(true);

    // Configurando engine
    engine.playAudio2D(AUDIO_CHANNEL_0, "assets/audio/Asteroids/space.wav", true);
    engine.setAudioVolume(AUDIO_CHANNEL_0, 0.5f);

    Engine::setCameraPosition({0, 0, 0});
    Engine::getCamera().setOrbitalCamera(true);

    irrklang::ISound* naveSound = NULL;
    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
            
        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

        if(Engine::keyPress(GLFW_KEY_W)) {
            engine.playAudio2D(AUDIO_CHANNEL_1, "assets/audio/Asteroids/thrust.wav");
        }
    
    });

    engine.setUpdateCallback([&](float dt) -> void {
        
    });

    engine.setRenderCallback([&](float dt) -> void {
        Texture.bind();
        Default.bind();
        sphere.getComponent<Transform>()->pushMatrix();
        sphere.render(Default);
        sphere.getComponent<AABBCollision3D>()->render(engine.getShader("collider"));
    });

    engine.run();

    return 0;
}