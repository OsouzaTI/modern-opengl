#include <iostream>
#include <queue>
#include <EngineCore.h>
#include <Model/SkinnedMesh.h>

Engine engine(1280, 720);

Shader DefaultShader[2];
SkinnedMesh malha;
Texture2D DefaultTexture;

int boneToRender = 1;

int main(void)
{
    DefaultShader[0]   = LOAD_SHADER("skinning/skeleton_debug", NULL, "DefaultShader");
    DefaultShader[1]   = LOAD_SHADER("skinning/skeleton_animation", NULL, "SkeletonAnimationShader");
    
    DefaultTexture  = LOAD_TEXTURE("bessa.jpg", false, "DefaultTexture");
    malha.loadMesh("assets/objects/human/bob.md5mesh");

    Engine::setCameraPosition({0, 0, 0});
    Engine::getCamera().setSpeed(10.0f);
    engine.setDebugMode(true);
    // Engine::getCamera().setOrbitalCamera(true);
    
    bool hasAnimation = malha.hasAnimations();
    int  animationIndex = 0;
    int  shaderIndex = 0;
    bool wireFrame = false;
    float scale = .5f;

    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
        
        if(wireFrame) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }

        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

        if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
            Engine::camera.processKeyboard(LEFT, deltaTime);
        } 
        
        if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
            Engine::camera.processKeyboard(RIGHT, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
            Engine::camera.processKeyboard(FORWARD, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
            Engine::camera.processKeyboard(BACKWARD, deltaTime);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {
        

    });

    engine.setRenderCallback([&](float dt) -> void {
        bool readOnlyHasAnimation = hasAnimation;
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

                ImGui::Begin("Settings");
                    ImGui::SliderInt("Bone render", &boneToRender, 1, malha.numOfBones());
                ImGui::End();

                ImGui::Begin("Model");
                    ImGui::Checkbox("Has animations", &readOnlyHasAnimation);
                    ImGui::Checkbox("Wireframe", &wireFrame);
                    ImGui::SliderFloat("Scale", &scale, 0, 1.0f);
                    ImGui::SliderInt("Shader", &shaderIndex, 0, ARRAY_SIZE_IN_ELEMENTS(DefaultShader)-1);
                    if(hasAnimation)
                    {
                        ImGui::SliderInt("Animation", &animationIndex, 0, malha.getNumAnimations()-1);
                    }
                ImGui::End();
            
            ImGui::EndFrame();
        }

        if(hasAnimation) {
            malha.setBoneTransform(DefaultShader[shaderIndex], glfwGetTime());
        }
    
        DefaultShader[shaderIndex].bind();
        DefaultTexture.bind();
        DefaultShader[shaderIndex].setMatrix4("projection", Engine::getProjectionMatrix());        
        DefaultShader[shaderIndex].setMatrix4("view", Engine::getViewMatrix());  
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, -2, 0));
        model = glm::scale(model, glm::vec3{scale});
        model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        // model = glm::rotate(model, (float)glm::radians(glfwGetTime()*10.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        DefaultShader[shaderIndex].setMatrix4("model", model);
        DefaultShader[shaderIndex].setInteger("boneToRender", boneToRender);
        malha.render();

    });

    engine.run();

    return 0;
}