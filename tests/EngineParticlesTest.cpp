#include <iostream>
#include <queue>
#include <EngineCore.h>

Engine engine(1280, 720);

EntityObject sphere;

int main(void)
{
    // Preparando assets
    engine.loadShader("particles/fire", "fireShader");
    engine.loadShader("model", "default");
    engine.loadTexture("dark_green_solid.png", "default", true);

    Shader& Default = engine.getShader("default");
    Shader& FireShader = engine.getShader("fireShader");
    Texture2D& Texture = engine.getTexture("default");

    sphere.loadModel("assets/objects/sphere/sphere02.obj");
    sphere.getComponent<Transform>()->setIdentityMatrix();
    sphere.getComponent<Transform>()->setPosition(glm::vec3{0, 0, -1.0f});
    sphere.getComponent<Transform>()->setScale(glm::vec3{0.1f});

    // adicionando componente de particulas    
    sphere.addComponent<ParticleEmitter>(5, PARTICLE_CUBE);
    ParticleEmitter* emitter = sphere.getComponent<ParticleEmitter>();
    
    emitter->setUpdateCallback([&](int index, Particle& p, float dt) {
        
        // fixando numa posição
        p.transform.setIdentityMatrix();
        p.transform.setPosition(sphere.getComponent<Transform>()->getPosition());
        p.transform.setScale(glm::vec3{0.025f});

        float angle = glm::radians(glfwGetTime() * 10.0f);
        p.transform.setPosition({
            (int)((index+1) * 5.0f) * cosf(angle), 
            (int)((index+1) * 5.0f) * sinf(angle), 
            (int)((index+1) * 5.0f) * -cosf(angle)
        });
        
   
    });

    engine.addEntity(&sphere);

    Engine::setCameraPosition({0, 0, 2});
    Engine::getCamera().setOrbitalCamera(true);

    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
            
        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }
    

        if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
            Engine::getCamera().processKeyboard(LEFT, deltaTime);
        } 
        
        if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
            Engine::getCamera().processKeyboard(RIGHT, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
            Engine::getCamera().processKeyboard(FORWARD, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
            Engine::getCamera().processKeyboard(BACKWARD, deltaTime);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {

    });

    engine.setRenderCallback([&](float dt) -> void {
        
        Texture.bind();
        Default.bind();
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        sphere.render(Default);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    });

    engine.run();

    return 0;
}