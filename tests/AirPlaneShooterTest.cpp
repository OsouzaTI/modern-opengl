#include <iostream>
#include <queue>
#include <EngineCore.h>
#include <Model/BasicMesh.h>
#include <Components/ParticleEmitter.h>
#include <Scene/SkyBox.h>
#include <Components/Transform.h>
#include <glm/gtc/random.hpp>

Engine engine(1280, 720);

#define N_ASTEROIDS 5

BasicMesh nave;
BasicMesh project;
DirectionalLight light{1.0f, {0, 0, -1}, {1.0f, 1.0f, 1.0f}};
SkyBox skybox;
glm::vec4 fogColor{0.0f, 0.0f, 0.0f, 1.0f};

Shader skyboxShader;
Shader lightShader;
Shader fogShader;
Texture2D defaultTexture;

struct Projectil {
    bool active = true;
    glm::vec3 position;
    glm::vec3 direction;
    glm::mat4 model;
    Projectil(glm::vec3 position, glm::vec3 direction)
        : position(position), direction(direction)
    {}

    void update(float dt)
    {
        position += direction * 5.0f * dt;
    }

    bool isOutsideScreen()
    {
        return (position.x < -10.0f || position.x > 10.0f || position.z < -10 || position.z > 10);
    }

    glm::mat4 getModel()
    {
        model = glm::mat4{1.0f};
        model = glm::translate(model, position);
        model = glm::scale(model, glm::vec3{0.05f});
        return model;
    }

};

struct AirPlane {

    float yaw = -90.0f;
    float pitch = 0.0f;

    float speed = 5.0f;
    float activeSpeed = 0.0f;
    float accelerate = 2.0f;
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 worldUp;
    glm::mat4 model;

    std::vector<Projectil> projectils;

    AirPlane(glm::vec3 position) 
        : position(position), front({0, 0, -1}), right({1, 0, 0}),
        worldUp({0, 1, 0}), up(worldUp)
    {}

    void update(float dt)
    {
        updateVectors();
        process(dt);
        movement(dt);

        // verificando elementos
        std::vector<Projectil> copy = projectils;
        projectils.clear();
        for (Projectil& p : copy)
        {
            if(!p.isOutsideScreen() && p.active) {
                p.update(dt);
                projectils.push_back(p);
            }
        }

    }

    void process(float dt)
    {
        
        // Acelerador
        if(Engine::keyPress(GLFW_KEY_W))
        {
            activeSpeed += accelerate * dt;
            activeSpeed = std::clamp(activeSpeed, 0.0f, speed);
        }

        if(Engine::keyPress(GLFW_KEY_A))
        {
            yaw -= 120.0f * dt;
        }

        if(Engine::keyPress(GLFW_KEY_D))
        {
            yaw += 120.0f * dt;
        }

        if(!Engine::keyPress(GLFW_KEY_W)) {
            activeSpeed -= accelerate * dt;
            activeSpeed = std::clamp(activeSpeed, 0.0f, speed);
        }

        // Limitando uma tela sem fim
        float horizontalOffset = 8.0f;
        float verticalOffset = 5.0f;
        if(position.x > horizontalOffset) {
            position.x = -horizontalOffset;
        } else if (position.x < -horizontalOffset) {
            position.x = horizontalOffset;
        }

        if(position.z > verticalOffset) {
            position.z = -verticalOffset;
        } else if(position.z < -verticalOffset) {
            position.z = verticalOffset;
        }

        // disparando projetil
        if(Engine::keyPress(GLFW_KEY_ENTER))
        {
            engine.playAudio2D(AUDIO_CHANNEL_2, "assets/audio/Asteroids/fire.wav");
            projectils.push_back(Projectil(position, front));
            Engine::keyUnPress(GLFW_KEY_ENTER);
        }

    }

    void movement(float dt)
    {
        position += front * activeSpeed * dt;
    }

    void updateVectors()
    {        
        glm::vec3 nFront = {
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
        };
        front = glm::normalize(nFront);
        right = glm::normalize(glm::cross(front, worldUp));
        up = glm::normalize(glm::cross(right, front));
    }

    glm::mat4 getModel()
    {
        model = glm::mat4{1.0f};
        model = glm::translate(model, position);
        model = glm::scale(model, glm::vec3{0.5f});
        model = glm::rotate(model, -glm::radians(90.0f + yaw), {0, 1, 0});        
        return model;
    }

};

struct RotateCamera 
{
    float yaw = -90.0f;
    float pitch = 0.0f;
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 worldUp;
    float offset = 4.0f;
    AirPlane& airPlane;

    RotateCamera(AirPlane& airPlane)
        : airPlane(airPlane), position(airPlane.position + glm::vec3(0, 0, offset)), front({0, 0, -1}), right({1, 0, 0}),
        worldUp({0, 1, 0}), up(worldUp)
    {

    }

    void update(float dt)
    {
        updateVectors(dt);
    }

    void updateVectors(float dt)
    {
        glm::vec3 nFront = {
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
        };
        front = glm::normalize(nFront);
        right = glm::normalize(glm::cross(front, worldUp));
        up = glm::normalize(glm::cross(right, front));

        // position = airPlane.position + glm::vec3(2, 10, 0);
    }

    glm::mat4 getView()
    {
        glm::mat4 view = glm::lookAt(glm::vec3(0, 10, -1), glm::vec3{0, -1, 0}, up);        
        return view;
    }

};

AirPlane airPlane({0, 0, -2});
RotateCamera camera(airPlane);

class Asteroid : public EngineObject {
private:
    #define N_STATES 4
    uint state = 1;
    EntityObject entities[N_STATES];
    bool alive[N_STATES]{1, 1, 1, 1};
    glm::vec3 direction[N_STATES];
    float scale[N_STATES]{0.15, 0.13, 0.10, 0.08};
public:

    Asteroid() {
        glm::vec3 axis{1.0, 0.0, 1.0};
        for (uint i = 0; i < N_STATES; i++)
        {
            entities[i].loadModel("assets/objects/Asteroids/rock_001_1.obj");
            entities[i].getComponent<Transform>()->setPosition(glm::ballRand(1.0f)*axis);
            entities[i].getComponent<Transform>()->setScale(glm::vec3{scale[i]});
            direction[i] = glm::normalize(glm::ballRand(1.0f));
            direction[i].y = 0;
        }
    }

    void process() override {

        // detectando colisões
        for(auto& projectil : airPlane.projectils) {
            glm::vec3 projectilPosition = projectil.position;
            for (uint i = 0; i < state; i++)
            {
                if(!alive[i]) {
                    continue;
                }

                bool isColliding = entities[i].getComponent<AABBCollision3D>()->isColliding(projectilPosition);
                if(isColliding) {

                    // audio da explosão
                    if(state == 1) {
                        engine.playAudio2D(AUDIO_CHANNEL_2, "assets/audio/Asteroids/bangLarge.wav");
                    } else if(state == 2) {
                        engine.playAudio2D(AUDIO_CHANNEL_2, "assets/audio/Asteroids/bangMedium.wav");
                    } else {
                        engine.playAudio2D(AUDIO_CHANNEL_2, "assets/audio/Asteroids/bangSmall.wav");
                    }

                    if (state == N_STATES) {
                        alive[i] = false;
                    } else {
                        state++;
                        // houve a colisão - Atualizando a posição dos novos asteroids
                        // com a posição do asteroid pai (estado atual)
                        glm::vec3 asteroidPosition = entities[i].getComponent<Transform>()->getPosition();
                        // transição
                        entities[state-1].getComponent<Transform>()->setPosition(asteroidPosition);                        
                        std::cout << "FOI PARA O ESTADO " << state << std::endl;
                    }
                    projectil.active = false;
                    break;
                }                    
            }
        }

        // Limitando uma tela sem fim
        for (uint i = 0; i < state; i++)
        {
            if(alive[i]) {
                infiniteScreen(i);
            }
        }            

    }

    void update(float dt) override {
        for (uint i = 0; i < state; i++)
        {
            glm::vec3 position = entities[i].getComponent<Transform>()->getPosition();
            if(alive[i]) {
                position += direction[i] * dt * 2.0f;
                entities[i].getComponent<Transform>()->setPosition(position);
            }
        }            
    }

    void render(Shader& shader) override {
        
    }

    void render(glm::mat4 viewMatix, Shader& shader) override {
        for (uint i = 0; i < state; i++)
        {
            if(alive[i]) {
                defaultTexture.bind();
                lightShader.bind();
                lightShader.setDirectionalLight(light);
                entities[i].getComponent<Transform>()->pushMatrix();
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                entities[i].render(viewMatix, shader);
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                entities[i].getComponent<AABBCollision3D>()->render(camera.getView(), engine.getShader("collider"));                
            }
        }     
    }

    void infiniteScreen(int asteroidIndex) {
        glm::vec3 position = entities[asteroidIndex].getComponent<Transform>()->getPosition();
        float horizontalOffset = 8.0f;
        float verticalOffset = 5.0f;
        if(position.x > horizontalOffset) {
            position.x = -horizontalOffset;
        } else if (position.x < -horizontalOffset) {
            position.x = horizontalOffset;
        }

        if(position.z > verticalOffset) {
            position.z = -verticalOffset;
        } else if(position.z < -verticalOffset) {
            position.z = verticalOffset;
        }
        entities[asteroidIndex].getComponent<Transform>()->setPosition(position);
    }

};


int main(void)
{
    Asteroid asteroid[N_ASTEROIDS];
    engine.loadShader("fog/model_fog", "default");
    engine.loadShader("light/directional_light", "lightShader");
    engine.loadShader("particles/fire", "fireShader");
    engine.loadShader("skybox/skybox", "SkyBox");

    engine.loadTexture("dark_green_solid.png", "default", true);
    engine.loadTexture("dark_red_solid.png", "cubeTexture", true);
    
    project.loadMesh("assets/objects/sphere/sphere02.obj");
    nave.loadMesh("assets/objects/airplane/airplane.obj");
    
    for (uint i = 0; i < N_ASTEROIDS; i++)
    {
        engine.addEntity(&asteroid[i]);
    }
    
    // Audio
    engine.playAudio2D(AUDIO_CHANNEL_0, "assets/audio/Asteroids/space.wav", true);
    engine.setAudioVolume(AUDIO_CHANNEL_0, 0.5f);

    // carregando as faces em um vetor
    std::vector<std::string> faces = {
        "assets/textures/skybox/4096/nx.png",
        "assets/textures/skybox/4096/nz.png",
        "assets/textures/skybox/4096/px.png",
        "assets/textures/skybox/4096/py.png",
        "assets/textures/skybox/4096/nx.png",
        "assets/textures/skybox/4096/nz.png",
    };

    skybox.loadCubeMap(faces);

    engine.setDebugMode(true);
    Engine::getCamera().setOrbitalCamera(true);
    engine.setProcessCallback([&](GLFWwindow* window, float dt){

        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

        if(Engine::keyPress(GLFW_KEY_1)) {
            engine.setHideMouse(true);
        }

        if(Engine::keyPress(GLFW_KEY_2)) {
            engine.setHideMouse(false);
        }

        // light.direction = glm::vec3{
        //     10 * cosf(glm::radians(glfwGetTime()*10.0f)),
        //     0, 
        //     10 * sinf(glm::radians(glfwGetTime()*10.0f))
        // };

    });

    engine.setUpdateCallback([&](float dt) -> void {
        airPlane.update(dt);
        camera.update(dt);
    });

    skyboxShader = engine.getShader("SkyBox");
    lightShader  = engine.getShader("lightShader");
    fogShader    = engine.getShader("default");
    defaultTexture = engine.getTexture("default");

    engine.setRenderCallback([&](float dt) -> void {
        int projectSize = airPlane.projectils.size();
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
            
                ImGui::Begin("Airplane");
                    ImGui::InputFloat3("Camera Position", &camera.position[0]);
                    ImGui::Separator();
                    ImGui::Text("Air Plane");
                    ImGui::InputInt("Projects", &projectSize);
                    ImGui::InputFloat("Yaw Airplane", &airPlane.yaw);
                    ImGui::InputFloat3("Front", &airPlane.front[0]);
                    ImGui::InputFloat3("Position", &airPlane.position[0]);
                    ImGui::SliderFloat("Active Speed", &airPlane.activeSpeed, 0.0f, airPlane.speed);
                    ImGui::Separator();
                    ImGui::SliderFloat4("Fog color", &fogColor[0], 0.0f, 1.0f);
                ImGui::End();

            ImGui::EndFrame();
        }

        // SKYBOX
        {
            skybox.getTransform().setIdentityMatrix();
            skybox.getTransform().setPosition({0, 0, 0});
            skybox.getTransform().setScale(glm::vec3{20});        
            skybox.getTransform().setRotate(glm::radians(glfwGetTime()), {1, 0, 1});
            skybox.getTransform().setRotate(glm::radians(airPlane.activeSpeed), {1, 0, 0});

            skyboxShader.bind()
                .setMatrix4("projection", Engine::getProjectionMatrix())
                .setMatrix4("view", camera.getView())
                .setMatrix4("model", skybox.getTransform().getModelMatrix());
            skybox.render();
        }

        { // Aviao
            defaultTexture.bind();
            lightShader.bind()
                .setDirectionalLight(light)
                .setMatrix4("projection", Engine::getProjectionMatrix())       
                .setMatrix4("view", camera.getView())
                .setMatrix4("model", airPlane.getModel());
            nave.render();

            for(Projectil& p : airPlane.projectils)
            {
                lightShader.setMatrix4("model", p.getModel());
                project.render();
            }

        }

        for (uint i = 0; i < N_ASTEROIDS; i++){
            asteroid[i].render(camera.getView(), lightShader);
        }


    });

    engine.run();

    return 0;
}