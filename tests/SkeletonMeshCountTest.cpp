#include <iostream>
#include <queue>
#include <EngineCore.h>
#include <Model/BasicMesh.h>
#include <Model/BasicBone.h>

std::vector<VertexBoneData> vertexToBones;
std::vector<int> meshBaseVertex;
std::map<std::string, uint> boneNameToIndexMap;

int getBoneId(const aiBone* bone)
{
    int boneId = 0;
    std::string boneName(bone->mName.C_Str());
    if(boneNameToIndexMap.find(boneName) == boneNameToIndexMap.end())
    {
        boneId = boneNameToIndexMap.size();
        boneNameToIndexMap[boneName] = boneId;
    } else {
        boneId = boneNameToIndexMap[boneName];
    }

    return boneId;
}

void parseSingleBone(uint meshId, const aiBone* bone)
{
    std::cout << "\t\tBone " << std::endl;
    std::cout << "\t\t  name: " << bone->mName.C_Str() << std::endl;
    std::cout << "\t\t  vertices affected: " << bone->mNumWeights << std::endl;

    int boneId = getBoneId(bone);
    for (uint i = 0; i < bone->mNumWeights; i++)
    {
        const aiVertexWeight& vw = bone->mWeights[i];
        uint globalVertexId = meshBaseVertex[meshId] + vw.mVertexId;
        std::cout << "Vertex global id " << globalVertexId << std::endl;
        assert(globalVertexId < vertexToBones.size());
        vertexToBones[globalVertexId].addBoneData(boneId, vw.mWeight);
    }   
    

}

void parseMeshBones(uint meshId, const aiMesh* mesh)
{
    for (uint i = 0; i < mesh->mNumBones; i++)
    {
        parseSingleBone(meshId, mesh->mBones[i]);
    }  
}

void parseMeshes(const aiScene* scene)
{
    std::cout << "Parsing " << scene->mNumMeshes << " meshes " << std::endl;

    uint totalVertices = 0;
    uint totalIndices = 0;
    uint totalBones = 0;

    meshBaseVertex.resize(scene->mNumMeshes);

    for (uint i = 0; i < scene->mNumMeshes; i++)
    {
        const aiMesh* mesh = scene->mMeshes[i];
        int numVertices = mesh->mNumVertices;
        int numIndices = mesh->mNumFaces * 3;
        int numBones = mesh->mNumBones;
        meshBaseVertex[i] = totalVertices;
        std::cout << "Mesh " << i << std::endl;
        std::cout << "\tVertices : " << numVertices << std::endl;
        std::cout << "\tIndices  : " << numIndices << std::endl;
        std::cout << "\tBones    : " << numBones << std::endl;
        totalVertices += numVertices;
        totalIndices  += numIndices;
        totalBones    += numBones;

        vertexToBones.resize(totalVertices);
        if(mesh->HasBones()) parseMeshBones(i, mesh);
    }

    std::cout << "Total Vertices : " << totalVertices << std::endl;  
    std::cout << "Total Indices  : " << totalIndices << std::endl;
    std::cout << "Total Bones    : " << totalBones << std::endl;

}



int main()
{

    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(
        "assets/objects/human/low_poly_character_rigged.dae", ASSIMP_LOAD_FLAGS);

    parseMeshes(scene);

    return 0;
}