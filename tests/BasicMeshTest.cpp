#include <iostream>
#include <queue>
#include <EngineCore.h>
#include <Structures/BasicMesh.h>

Engine engine(1280, 720);

Shader DefaultShader;
BasicMesh malha01;
AssimpModel malha02;
BasicMesh malha03;
Texture2D DefaultTexture;

int main(void)
{
    DefaultShader   = LOAD_SHADER("model", NULL, "DefaultShader");
    DefaultTexture  = LOAD_TEXTURE("bessa.jpg", false, "DefaultTexture");
    malha01.loadMesh("assets/objects/Skull/12140_Skull_v3_L2.obj");
    malha02.loadModel("assets/objects/cube/cube.obj");
    malha03.loadMesh("assets/objects/dragon/Dragon 2.5_fbx.fbx");

    Engine::setCameraPosition({0, 0, 0});

    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
            
        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {

    });

    engine.setRenderCallback([&](float dt) -> void {
        
        DefaultShader.bind();
        DefaultTexture.bind();
        DefaultShader.setMatrix4("projection", Engine::getProjectionMatrix());        
        DefaultShader.setMatrix4("view", Engine::getViewMatrix());  
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0, 0, -30));
        model = glm::scale(model, glm::vec3{0.2f, 0.2f, 0.2f});
        model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        model = glm::rotate(model, (float)glm::radians(glfwGetTime()*10.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        DefaultShader.setMatrix4("model", model);
        malha03.render();

        // DefaultShader.bind();
        // DefaultShader.setMatrix4("projection", Engine::getProjectionMatrix());        
        // DefaultShader.setMatrix4("view", Engine::getViewMatrix());  
        // model = glm::mat4(1.0f);
        // model = glm::translate(model, glm::vec3(5, -5, -20));
        // model = glm::scale(model, glm::vec3{.2f, .2f, .2f});
        // model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        // model = glm::rotate(model, (float)glm::radians(glfwGetTime()*10.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        // DefaultShader.setMatrix4("model", model);
        // malha01.render();

        // DefaultShader.bind();
        // DefaultTexture.bind();
        // DefaultShader.setMatrix4("projection", Engine::getProjectionMatrix());        
        // DefaultShader.setMatrix4("view", Engine::getViewMatrix());  
        
        // model = glm::mat4(1.0f);
        // model = glm::translate(model, glm::vec3(-5, -5, -20));
        // model = glm::scale(model, glm::vec3{.9f, .9f, .9f});
        // model = glm::rotate(model, (float)glm::radians(-90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
        // model = glm::rotate(model, (float)glm::radians(glfwGetTime()*10.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        // DefaultShader.setMatrix4("model", model);
        // malha02.render(DefaultShader);



    });

    engine.run();

    return 0;
}