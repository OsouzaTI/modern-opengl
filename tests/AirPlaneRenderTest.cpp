#include <iostream>
#include <queue>
#include <EngineCore.h>
#include <Model/BasicMesh.h>
#include <Components/ParticleEmitter.h>
#include <Scene/SkyBox.h>
// https://free3d.com/3d-model/plane-262015.html

Engine engine(1280, 720);

BasicMesh malha, reference, buildings[10];
ParticleEmitter emitter(5, PARTICLE_CUBE);
DirectionalLight light{1.0f, {0, 0, -1}, {1.0f, 1.0f, 1.0f}};
SkyBox skybox;

glm::vec4 fogColor{0.0f, 0.0f, 0.0f, 1.0f};

float yaw = -90.0f;
float pitch = 0.0f;
float roll = 0.0f;

struct AirPlane {

    float yaw = -90.0f;
    float pitch = 0.0f;

    float speed = 20.5f;
    float activeSpeed = 0.0f;
    float accelerate = 5.3f;
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 worldUp;
    glm::mat4 model;

    AirPlane(glm::vec3 position) 
        : position(position), front({0, 0, -1}), right({1, 0, 0}),
        worldUp({0, 1, 0}), up(worldUp)
    {}

    void update(float dt)
    {
        updateVectors();
        process(dt);
        movement(dt);
    }

    void process(float dt)
    {
        // Acelerador
        if(Engine::keyPress(GLFW_KEY_W))
        {
            activeSpeed += accelerate * dt;
            activeSpeed = std::clamp(activeSpeed, 0.0f, speed);
        }

        if(!Engine::keyPress(GLFW_KEY_W)) {
            activeSpeed -= accelerate * dt;
            activeSpeed = std::clamp(activeSpeed, 0.0f, speed);
        }

    }

    void movement(float dt)
    {
        position += front * activeSpeed * dt;
    }

    void updateVectors()
    {
        yaw     = Engine::getCamera().getYaw() * 0.7f;
        pitch   = Engine::getCamera().getPitch() * 0.7f;
        glm::vec3 nFront = {
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
        };
        front = glm::normalize(nFront);
        right = glm::normalize(glm::cross(front, worldUp));
        up = glm::normalize(glm::cross(right, front));
    }

    glm::mat4 getModel()
    {
        model = glm::mat4{1.0f};
        model = glm::translate(model, position);
        model = glm::rotate(model, -glm::radians(90.0f + yaw), {0, 1, 0});
        model = glm::rotate(model, glm::radians(pitch), {1, 0, 0});
        return model;
    }

};

struct RotateCamera 
{
    glm::vec3 position;
    glm::vec3 front;
    glm::vec3 right;
    glm::vec3 up;
    glm::vec3 worldUp;
    float offset = 4.0f;
    AirPlane& airPlane;

    RotateCamera(AirPlane& airPlane)
        : airPlane(airPlane), position(airPlane.position + glm::vec3(0, 0, offset)), front({0, 0, -1}), right({1, 0, 0}),
        worldUp({0, 1, 0}), up(worldUp)
    {

    }

    void update(float dt)
    {
        updateVectors(dt);
    }

    void updateVectors(float dt)
    {
        glm::vec3 nFront = {
            cos(glm::radians(yaw)) * cos(glm::radians(pitch)),
            sin(glm::radians(pitch)),
            sin(glm::radians(yaw)) * cos(glm::radians(pitch))
        };
        front = glm::normalize(nFront);
        right = glm::normalize(glm::cross(front, worldUp));
        up = glm::normalize(glm::cross(right, front));

        // atualizando a posição da camera em ralação ao aviao
        float angle = 
        glm::acos(glm::dot(position, airPlane.position) / 
            (glm::length(position) * glm::length(airPlane.position)));
        // glm::radians(glfwGetTime() * 10.0f);
        float radius = offset;
        float inv = (airPlane.front.z > 0.0f) ? -1 : 1;
        position = (airPlane.position + inv * glm::vec3(0, 0, offset))
            + glm::vec3(
                -radius * sinf(glm::radians(90.0f + airPlane.yaw * 1.1f)),
                -radius * sinf(glm::radians(airPlane.pitch)),
                radius * sinf(angle));
    }

    glm::mat4 getView()
    {
        glm::mat4 view = glm::lookAt(position, airPlane.position + airPlane.front, up);        
        return view;
    }

};

AirPlane airPlane({0, 10, -2});
RotateCamera camera(airPlane);

int main(void)
{
    engine.loadShader("fog/model_fog", "default");
    engine.loadShader("light/directional_light", "lightShader");
    engine.loadShader("particles/fire", "fireShader");
    engine.loadShader("skybox/skybox", "SkyBox");

    engine.loadTexture("dark_green_solid.png", "default", true);
    engine.loadTexture("dark_red_solid.png", "cubeTexture", true);

    malha.loadMesh("assets/objects/airplane/airplane.obj");
    reference.loadMesh("assets/objects/cube/cube.obj");
    
    buildings[0].loadMesh("assets/objects/city/Amaryllis City.obj");
    

    EntityObject cubo;
    cubo.loadModel("assets/objects/cube/cube.obj");
    cubo.addComponent<Transform>(glm::vec3{0, 0, 0});
    engine.addEntity(cubo);

    // carregando as faces em um vetor
    std::vector<std::string> faces = {
        "assets/textures/skybox/4096/nx.png",
        "assets/textures/skybox/4096/nz.png",
        "assets/textures/skybox/4096/px.png",
        "assets/textures/skybox/4096/py.png",
        "assets/textures/skybox/4096/nx.png",
        "assets/textures/skybox/4096/nz.png",
    };

    skybox.loadCubeMap(faces);

    emitter.setUpdateCallback([&](int index, Particle& p, float dt) {
        p.transform.setModelMatrix(airPlane.getModel());
        p.transform.setScale({.015, .015, .015});
        p.transform.setPosition({0, 0, 20});
        p.transform.setPosition(p.transform.getPosition() + glm::vec3{0, 0, 1});

        float mn = 20.0f * (rand()%(index+1 * 10));
        p.transform.setPosition(
            glm::vec3{
                (index+1*2.5f) * cosf(glm::radians(glfwGetTime()*mn)), 
                (index+1*2.5f) * sinf(glm::radians(glfwGetTime()*mn)), 
                (index+1*2.5f) * 5.0f
            }
        );
    });

    engine.setDebugMode(true);
    Engine::getCamera().setOrbitalCamera(true);
    engine.setProcessCallback([&](GLFWwindow* window, float dt){

        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

        if(Engine::keyPress(GLFW_KEY_1)) {
            engine.setHideMouse(true);
        }

        if(Engine::keyPress(GLFW_KEY_2)) {
            engine.setHideMouse(false);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {
        airPlane.update(dt);
        camera.update(dt);
        emitter.update(dt);
        // emitter.setNumOfParticles((int)map(airPlane.activeSpeed, 0.0f, airPlane.speed, 10.0f, 20.0f));
    });

    Shader& skyboxShader = engine.getShader("SkyBox");
    Shader& lightShader  = engine.getShader("lightShader");
    Shader& fogShader    = engine.getShader("default");
    Texture2D& defaultTexture = engine.getTexture("default");

    engine.setRenderCallback([&](float dt) -> void {
    
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();

                ImGui::Begin("Airplane");
                    ImGui::SliderFloat("Yaw", &yaw, -180.0f, 0.0f);
                    ImGui::SliderFloat("Pitch", &pitch, -180.0f, 180.0f);
                    ImGui::SliderFloat("Roll", &roll, -180.0f, 180.0f);
                    ImGui::InputFloat3("Position", &camera.position[0]);
                    ImGui::Separator();
                    ImGui::Text("Air Plane");
                    ImGui::InputFloat("Yaw Airplane", &airPlane.yaw);
                    ImGui::InputFloat3("Front", &airPlane.front[0]);
                    ImGui::InputFloat3("Position", &airPlane.position[0]);
                    ImGui::SliderFloat("Active Speed", &airPlane.activeSpeed, 0.0f, airPlane.speed);
                    ImGui::Separator();
                    ImGui::SliderFloat4("Fog color", &fogColor[0], 0.0f, 1.0f);
                ImGui::End();
            
            ImGui::EndFrame();
        }

        // SKYBOX
        {
            // skybox.getTransform().setIdentityMatrix();
            // skybox.getTransform().setPosition(airPlane.position);
            // skybox.getTransform().setScale(glm::vec3{90});        
            // skybox.getTransform().setRotate(glm::radians(glfwGetTime()), {1, 0, 1});
            
            // skyboxShader.bind()
            //     .setMatrix4("projection", Engine::getProjectionMatrix())
            //     .setMatrix4("view", camera.getView())
            //     .setMatrix4("model", skybox.getTransform().getModelMatrix());
            // skybox.render();
        }

        { // Aviao
            defaultTexture.bind();
            lightShader.bind()
                .setDirectionalLight(light)
                .setMatrix4("projection", Engine::getProjectionMatrix())       
                .setMatrix4("view", camera.getView())
                .setMatrix4("model", airPlane.getModel());
            malha.render();

            if(airPlane.activeSpeed > 0) {            
                // emitter.render(camera.getView(), engine.getShader("fireShader"));
            }

        }

        { // Cidade
            glm::mat4 model{1.0f};
            model = glm::translate(model, {0, 0, -20});
            model = glm::scale(model, glm::vec3{.009f});
            
            defaultTexture.bind();            
            fogShader.bind()
                // start fog
                .setVector3f("cameraEye", airPlane.position)
                .setVector4f("fogColor", fogColor)
                // end fog
                .setMatrix4("projection", Engine::getProjectionMatrix())    
                .setMatrix4("view", camera.getView())  
                .setMatrix4("model", model);
            buildings[0].render();
        }

    });

    engine.run();

    return 0;
}