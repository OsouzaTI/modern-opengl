#include <iostream>
#include <queue>
#include <EngineCore.h>

Engine engine(1280, 720);

EntityObject sphere;

int main(void)
{
    engine.loadShader("model", "default");
    engine.loadTexture("dark_green_solid.png", "default", true);

    Shader& Default = engine.getShader("default");
    Texture2D& Texture = engine.getTexture("default");

    sphere.loadModel("assets/objects/sphere/sphere02.obj");
    sphere.setRenderCollider(true);

    Engine::setCameraPosition({0, 0, 0});

    Engine::getCamera().setOrbitalCamera(true);
    engine.setProcessCallback([&](GLFWwindow* window, float deltaTime){
            
        if(Engine::keyPress(GLFW_KEY_ESCAPE)) {
            glfwSetWindowShouldClose(window, true);
        }

        if(Engine::keyPress(GLFW_KEY_A) || Engine::keyPress(GLFW_KEY_LEFT)) {
            Engine::getCamera().processKeyboard(LEFT, deltaTime);
        } 
        
        if(Engine::keyPress(GLFW_KEY_D) || Engine::keyPress(GLFW_KEY_RIGHT)) {
            Engine::getCamera().processKeyboard(RIGHT, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_W) || Engine::keyPress(GLFW_KEY_UP)) {
            Engine::getCamera().processKeyboard(FORWARD, deltaTime);
        }

        if(Engine::keyPress(GLFW_KEY_S) || Engine::keyPress(GLFW_KEY_DOWN)) {
            Engine::getCamera().processKeyboard(BACKWARD, deltaTime);
        }

    });

    engine.setUpdateCallback([&](float dt) -> void {
        
    });

    engine.setRenderCallback([&](float dt) -> void {
        Texture.bind();
        Default.bind();
        sphere.getComponent<Transform>()->pushMatrix();
        sphere.render(Default);
        sphere.getComponent<AABBCollision3D>()->render(engine.getShader("collider"));
    });

    engine.run();

    return 0;
}